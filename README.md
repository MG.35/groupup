# <image src="src/main/resources/static/logo.png" height="50px" /> GroupUp 

GroupUp is a solution for communication within a community. GroupUp allows community users to share thoughts and ideas on a shared dashboard.

# Architecture
The architecture is visible in the [wiki](https://gitlab.com/MG.35/groupup/-/wikis/Architecture).

# Instructions to use GroupUp
## Requirements
- PostgreSQL 14.2

- Java 17.0.2

- Maven 3.8.1

## Run the application with docker
To run docker you must first build the jar file using `mvn spring-boot:build-image` then run
`docker-compose build; docker-compose up`. The default port is 8080.

## Run the application without docker
The application can be started without docker with the following command:
```
mvn spring-boot:run
```
**NOTE**: You need to have a PostgreSQL database running at the URL specified in the `application.properties` file.

In the localhost environment, the application starts by default at port **8080**.
The port can be changed in the `application.properties` file.

## Run a localhost database
It is possible to use docker-compose to start a Postgres database that can be used by the application.
Use the following command to start the database:
```
docker-compose -f database.yml up -d
```
## Use your own database
If you want to use your own database, you can edit the `application.preperties` in the `src/main/resources` folder, 
and change the `spring.datasource.url`, `spring.datasource.username` and `spring.datasource.password` properties.

## Sign-in
To sign-in, you can use one of the following accounts:
```
username: frances
password: password
```
```
username: ivy
password: password
```
```
username: watt
password: password
```

# REST API

The root of the REST API is `/api`. The API uses a [HAL JSON](https://stateless.group/hal_specification.html) document format. 

# Development

## Run the tests
The test are divided into Spring tests that test the API, React tests that test the frontend and the validation 
tests that test the entire application using the API, frontend and database.

Spring unit tests are run with the following command:
```
mvn test -Dskip.API.test=false -Dtest=*UTest.java
```
Spring integration tests are run with the following command:
```
mvn test -Dskip.API.test=false -Dtest=*ITest.java
```

React tests are run with the following command:
```
npm run test -Dskip.REACT.test=false
```
Validation tests are run with the following command:
```
mvn test -Dskip.VALIDATION.test=false
```

## Documentation usage
Java class documentation is generated with [doxygen](https://www.doxygen.nl/). The doxygen configuration is located
in the `src/main/java` folder.

