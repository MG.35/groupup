import {
    queryByAttribute,
    render,
    waitFor,
    fireEvent,
    getByText,
    queryByTestId,
    prettyDOM,
    queryByText
} from "@testing-library/react"
import React from "react"
import {createServer, Model} from "miragejs"
import '@testing-library/jest-dom'
import UserContext from "../../../main/js/shared/user-context/user_context"
import getCommentRoot16 from "./mock-calls/gets/getCommentRoot16"
import Comment from "../../../main/js/shared/post/comment"
import getAuthor16 from "./mock-calls/gets/getAuthor16";

const commentRoot = getCommentRoot16
const commentRootAuthor = getAuthor16
const index = 0

let server

beforeEach(() => {
    server = createServer({
        models: {
            author: Model
        },
        routes() {
            this.urlPrefix = 'http://127.0.0.1:8080';
            this.get("/api/commentRoots/16/author", commentRootAuthor);
        }
    })
});

afterEach(() => {
    server.shutdown();
});

const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

function renderWithContext() {
    return (
        <UserContext.Provider value={user}>
            <Comment key={index} comment={commentRoot}/>
        </UserContext.Provider>
    )
}

describe("Comment component tests:", () => {
        it("Single comment is displayed", () => {
            const { container } = render(renderWithContext());

            return waitFor(() => {
                expect(queryByTestId(container, 'commentRoot-16') == null).toBe(false);
                expect(queryByText(container, 'Willis Lyon') == null).toBe(false);
                // console.log(prettyDOM(container))
            })
        })
    }
)