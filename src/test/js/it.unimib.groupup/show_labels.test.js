import React from "react";
import {queryByAttribute, render, waitFor} from "@testing-library/react";
import UserContext from "../../../main/js/shared/user-context/user_context";
import SavedLabels from "../../../main/js/saved_posts_with_labels/saved_labels";
import '@testing-library/jest-dom';
import {MemoryRouter, Routes, Route} from "react-router-dom";
import {createServer, Model} from "miragejs";
import getApiUsers1MyLabels from "./mock-calls/gets/getApiUsers1MyLabels";

let server

beforeEach(() => {
    server = createServer({
        models: {
            post: Model,
            user: Model
        },
        routes() {
            this.get("/api/users/1/myLabels", () => {
                return getApiUsers1MyLabels
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

function renderComponentWithContext() {
    return (

        <UserContext.Provider value={user}>
            <MemoryRouter initialEntries={["/"]}>
                <Routes>
                    <Route path={"/"} element={<SavedLabels/>}/>
                </Routes>
            </MemoryRouter>
        </UserContext.Provider>
    )
}

describe("SavedLabels component tests:", () => {


        it("the page of saved labels is rendered", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderComponentWithContext());
                expect(getById(page.container, 'labels') == null).toBe(false);
                return waitFor(() => {
                    expect(getById(page.container, 'label-Motorsport') == null).toBe(false);
                });

            }
        )


    }
)
