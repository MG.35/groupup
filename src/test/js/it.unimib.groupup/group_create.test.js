import React from "react";
import {queryByAttribute, render, fireEvent} from "@testing-library/react";
import { createServer, Model } from "miragejs"
import GroupCreate from "../../../main/js/group/group_create";
import UserContext from "../../../main/js/shared/user-context/user_context";
import {MemoryRouter, Routes, Route} from "react-router-dom";

let server;

const user = {
    id: 1,
    name : "name1",
    surname : "surname1",
    email : "mail1",
    password : "pass1",
    username : "username1",
    photo : "photo1",
}

beforeEach(() => {
    server = createServer({
        models: {
            gs: Model
        },
        routes() {
            this.post("/api/myGroups", (schema, request) => {
                let attrs = JSON.parse(request.requestBody)
                schema.gs.create({name: attrs.name, description: attrs.description, users: attrs.users});
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

function renderUserWithContext() {
    return(
        <UserContext.Provider value={user}>
            <MemoryRouter initialEntries={["/"]}>
                <Routes>
                    <Route path={"/"} element={<GroupCreate />}/>
                </Routes>
            </MemoryRouter>
        </UserContext.Provider>
    )
}

describe("New Group creation component tests:", () => {

        it("Name and description text areas are displayed", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderUserWithContext());
                expect(getById(page.container, 'name_id') == null).toBe(false);
                expect(getById(page.container, 'description_id') == null).toBe(false);
            }
        )

        it("Create group button should be clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(renderUserWithContext());
            const test_name = "TEST_NAME"
            const test_descr = "TEST_DESCRIPTION"

            fireEvent.change(getById(page.container, "name_id"), {
                target: {value: test_name},
            });
            fireEvent.change(getById(page.container, "description_id"), {
                target: {value: test_descr},
            });

            fireEvent.click(getById(page.container, "button-c"));
            expect(server.db.gs[0].name).toBe(test_name);
            expect(server.db.gs[0].description).toBe(test_descr);
            expect(server.db.gs[0].users[0]).toBe("/api/users/" + user.id);
        })

    }
)

