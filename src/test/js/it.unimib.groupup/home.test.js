import React from "react";
import {queryByAttribute, render} from "@testing-library/react";
import UserContext from "../../../main/js/shared/user-context/user_context";
import Home from "../../../main/js/home/home";

const user = {
    id: 1,
    name : "name1",
    surname : "surname1",
    email : "mail1",
    password : "pass1",
    username : "username1",
    photo : "photo1",
}

function renderUserWithContext() {
    return(
        <UserContext.Provider value={user}>
            <Home />
        </UserContext.Provider>
    )
}

describe("Home component tests:", () => {


        it("Home component has sign-in button", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderUserWithContext());
                expect(getById(page.container, 'sign-in-button') == null).toBe(false);

            }
        )


    }
)