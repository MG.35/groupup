export default {
    "name": "name",
    "description": "description",
    "_links": {
        "self": {
            "href": "http://localhost:8080/api/myGroups/4"
        },
        "myGroup": {
            "href": "http://localhost:8080/api/myGroups/4"
        },
        "users": {
            "href": "http://localhost:8080/api/myGroups/4/users"
        },
        "posts": {
            "href": "http://localhost:8080/api/myGroups/4/posts"
        }
    }
};