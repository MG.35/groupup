export default {
    "_embedded" : {
    "savedPosts" : [ {
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/api/savedPosts/24"
            },
            "savedPost" : {
                "href" : "http://localhost:8080/api/savedPosts/24"
            },
            "user" : {
                "href" : "http://localhost:8080/api/savedPosts/24/user"
            },
            "label" : {
                "href" : "http://localhost:8080/api/savedPosts/24/label"
            },
            "post" : {
                "href" : "http://localhost:8080/api/savedPosts/24/post"
            }
        }
    } ]
},
    "_links" : {
    "self" : {
        "href" : "http://localhost:8080/api/labels/22/savedPosts"
    }
}
}