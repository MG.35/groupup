export default {
    "_embedded" : {
        "myGroups" : [ {
            "name" : "GroupName",
            "description" : "description",
            "photo" : null,
            "_links" : {
                "self" : {
                    "href" : "http://localhost:8080/api/myGroups/4"
                },
                "myGroup" : {
                    "href" : "http://localhost:8080/api/myGroups/4"
                },
                "users" : {
                    "href" : "http://localhost:8080/api/myGroups/4/users"
                },
                "posts" : {
                    "href" : "http://localhost:8080/api/myGroups/4/posts"
                }
            }
        } ]
    },
    "_links" : {
        "self" : {
            "href" : "http://localhost:8080/api/users/1/myGroups"
        }
    }
};