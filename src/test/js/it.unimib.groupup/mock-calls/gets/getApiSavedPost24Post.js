export default {
    "message" : "My favorite car is the one that has the most doors.",
    "date" : "2022-05-21T10:33:38.043+00:00",
    "_links" : {
    "self" : {
        "href" : "http://localhost:8080/api/posts/14"
    },
    "post" : {
        "href" : "http://localhost:8080/api/posts/14"
    },
    "likedBy" : {
        "href" : "http://localhost:8080/api/posts/14/likedBy"
    },
    "myGroup" : {
        "href" : "http://localhost:8080/api/posts/14/myGroup"
    },
    "author" : {
        "href" : "http://localhost:8080/api/posts/14/author"
    },
    "comments" : {
        "href" : "http://localhost:8080/api/posts/14/comments"
    },
    "savedPosts" : {
        "href" : "http://localhost:8080/api/posts/14/savedPosts"
    }
}
}