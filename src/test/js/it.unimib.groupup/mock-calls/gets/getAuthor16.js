export default {
    "name": "Willis",
    "surname": "Lyon",
    "email": "w.lyon@domain.com",
    "username": "frances",
    "photo": "/user1.jpg",
    "likedComments": [
        {
            "sentence": "messageCommentRoot2",
            "_links": {
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/1"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/3"
                    }
                ],
                "post": {
                    "href": "http://127.0.0.1:8080/api/posts/6"
                },
                "comments": {
                    "href": "http://127.0.0.1:8080/api/commentLeaves/20"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/2"
                }
            }
        },
        {
            "sentence": "messageCommentRoot3",
            "_links": {
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/1"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/2"
                    }
                ],
                "post": {
                    "href": "http://127.0.0.1:8080/api/posts/7"
                },
                "comments": {
                    "href": "http://127.0.0.1:8080/api/commentLeaves/21"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/3"
                }
            }
        },
        {
            "sentence": "messageCommentLeaf1",
            "comments": {
                "sentence": "messageCommentRoot1"
            },
            "_links": {
                "commentRoot": {
                    "href": "http://127.0.0.1:8080/api/commentRoots/16"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/3"
                },
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/1"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/2"
                    }
                ]
            }
        },
        {
            "sentence": "messageCommentLeaf2",
            "comments": {
                "sentence": "messageCommentRoot2"
            },
            "_links": {
                "commentRoot": {
                    "href": "http://127.0.0.1:8080/api/commentRoots/17"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/2"
                },
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/3"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/1"
                    }
                ]
            }
        }
    ],
    "myComments": [
        {
            "sentence": "messageCommentRoot1",
            "_links": {
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/2"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/3"
                    }
                ],
                "post": {
                    "href": "http://127.0.0.1:8080/api/posts/5"
                },
                "comments": {
                    "href": "http://127.0.0.1:8080/api/commentLeaves/19"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/1"
                }
            }
        },
        {
            "sentence": "messageCommentLeaf3",
            "comments": {
                "sentence": "messageCommentRoot3"
            },
            "_links": {
                "commentRoot": {
                    "href": "http://127.0.0.1:8080/api/commentRoots/18"
                },
                "author": {
                    "href": "http://127.0.0.1:8080/api/users/1"
                },
                "likedBy": [
                    {
                        "href": "http://127.0.0.1:8080/api/users/2"
                    },
                    {
                        "href": "http://127.0.0.1:8080/api/users/3"
                    }
                ]
            }
        }
    ],
    "roles": [],
    "_links": {
        "self": {
            "href": "http://127.0.0.1:8080/api/users/1"
        },
        "user": {
            "href": "http://127.0.0.1:8080/api/users/1"
        },
        "myGroups": {
            "href": "http://127.0.0.1:8080/api/users/1/myGroups"
        },
        "friends": {
            "href": "http://127.0.0.1:8080/api/users/1/friends"
        },
        "likedPost": {
            "href": "http://127.0.0.1:8080/api/users/1/likedPost"
        },
        "savedPosts": {
            "href": "http://127.0.0.1:8080/api/users/1/savedPosts"
        },
        "myLabels": {
            "href": "http://127.0.0.1:8080/api/users/1/myLabels"
        },
        "posts": {
            "href": "http://127.0.0.1:8080/api/users/1/posts"
        }
    }
}