export default {
    "sentence": "messageCommentRoot1",
    "_links": {
        "self": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16"
        },
        "commentRoot": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16"
        },
        "likedBy": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16/likedBy"
        },
        "post": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16/post"
        },
        "comments": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16/comments"
        },
        "author": {
            "href": "http://127.0.0.1:8080/api/commentRoots/16/author"
        }
    }
}