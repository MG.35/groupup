export default {
    "_embedded": {
        "posts": [{
            "message": "messagePost1",
            "_links": {
                "self": {
                    "href": "http://localhost:8080/api/posts/5"
                },
                "post": {
                    "href": "http://localhost:8080/api/posts/5"
                },
                "comments": {
                    "href": "http://localhost:8080/api/posts/5/comments"
                },
                "myGroup": {
                    "href": "http://localhost:8080/api/posts/5/myGroup"
                },
                "author": {
                    "href": "http://localhost:8080/api/posts/5/author"
                },
                "likedBy": {
                    "href": "http://localhost:8080/api/posts/5/likedBy"
                }
            }
        }]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/api/myGroups/4/posts"
        }
    }
};