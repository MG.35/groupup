export default {
    "_embedded": {
        "myLabels": [{
            "label": "Motorsport",
            "_links": {
                "self": {
                    "href": "http://localhost:8080/api/labels/22"
                },
                "myLabel": {
                    "href": "http://localhost:8080/api/labels/22"
                },
                "savedPosts": {
                    "href": "http://localhost:8080/api/labels/22/savedPosts"
                },
                "user": {
                    "href": "http://localhost:8080/api/labels/22/user"
                }
            }
        }]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/api/users/1/myLabels"
        }
    }
}