export default {
    "_embedded": {
    "users": [
        {
            "name": "Willis",
            "surname": "Lyon",
            "email": "w.lyon@domain.com",
            "password": "password",
            "username": "frances",
            "photo": "/user1.jpg",
            "likedComments": [
                {
                    "sentence": "messageCommentLeaf1",
                    "comments": {
                        "sentence": "messageCommentRoot1"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/3"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/16"
                        }
                    }
                },
                {
                    "sentence": "messageCommentRoot3",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/7"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/3"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/21"
                        }
                    }
                },
                {
                    "sentence": "messageCommentRoot2",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/6"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/2"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/20"
                        }
                    }
                },
                {
                    "sentence": "messageCommentLeaf2",
                    "comments": {
                        "sentence": "messageCommentRoot2"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/2"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/17"
                        }
                    }
                }
            ],
            "myComments": [
                {
                    "sentence": "messageCommentRoot1",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/5"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/1"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/19"
                        }
                    }
                },
                {
                    "sentence": "messageCommentLeaf3",
                    "comments": {
                        "sentence": "messageCommentRoot3"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/1"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/18"
                        }
                    }
                }
            ],
            "_links": {
                "self": {
                    "href": "http://127.0.0.1:8080/api/users/1"
                },
                "user": {
                    "href": "http://127.0.0.1:8080/api/users/1"
                },
                "myLabels": {
                    "href": "http://127.0.0.1:8080/api/users/1/myLabels"
                },
                "likedPost": {
                    "href": "http://127.0.0.1:8080/api/users/1/likedPost"
                },
                "posts": {
                    "href": "http://127.0.0.1:8080/api/users/1/posts"
                },
                "savedPosts": {
                    "href": "http://127.0.0.1:8080/api/users/1/savedPosts"
                },
                "myGroups": {
                    "href": "http://127.0.0.1:8080/api/users/1/myGroups"
                },
                "friends": {
                    "href": "http://127.0.0.1:8080/api/users/1/friends"
                }
            }
        },
        {
            "name": "Duncan",
            "surname": "Vernon",
            "email": "d.vernon@domain.com",
            "password": "password",
            "username": "watt",
            "photo": "/user3.jpg",
            "likedComments": [
                {
                    "sentence": "messageCommentRoot1",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/5"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/1"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/19"
                        }
                    }
                },
                {
                    "sentence": "messageCommentRoot2",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/6"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/2"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/20"
                        }
                    }
                },
                {
                    "sentence": "messageCommentLeaf3",
                    "comments": {
                        "sentence": "messageCommentRoot3"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/1"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/18"
                        }
                    }
                },
                {
                    "sentence": "messageCommentLeaf2",
                    "comments": {
                        "sentence": "messageCommentRoot2"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/3"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/2"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/17"
                        }
                    }
                }
            ],
            "myComments": [
                {
                    "sentence": "messageCommentLeaf1",
                    "comments": {
                        "sentence": "messageCommentRoot1"
                    },
                    "_links": {
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/3"
                        },
                        "commentRoot": {
                            "href": "http://127.0.0.1:8080/api/commentRoots/16"
                        }
                    }
                },
                {
                    "sentence": "messageCommentRoot3",
                    "_links": {
                        "post": {
                            "href": "http://127.0.0.1:8080/api/posts/7"
                        },
                        "likedBy": [
                            {
                                "href": "http://127.0.0.1:8080/api/users/1"
                            },
                            {
                                "href": "http://127.0.0.1:8080/api/users/2"
                            }
                        ],
                        "author": {
                            "href": "http://127.0.0.1:8080/api/users/3"
                        },
                        "comments": {
                            "href": "http://127.0.0.1:8080/api/commentLeaves/21"
                        }
                    }
                }
            ],
            "_links": {
                "self": {
                    "href": "http://127.0.0.1:8080/api/users/3"
                },
                "user": {
                    "href": "http://127.0.0.1:8080/api/users/3"
                },
                "myLabels": {
                    "href": "http://127.0.0.1:8080/api/users/3/myLabels"
                },
                "likedPost": {
                    "href": "http://127.0.0.1:8080/api/users/3/likedPost"
                },
                "posts": {
                    "href": "http://127.0.0.1:8080/api/users/3/posts"
                },
                "savedPosts": {
                    "href": "http://127.0.0.1:8080/api/users/3/savedPosts"
                },
                "myGroups": {
                    "href": "http://127.0.0.1:8080/api/users/3/myGroups"
                },
                "friends": {
                    "href": "http://127.0.0.1:8080/api/users/3/friends"
                }
            }
        }
    ]
},
    "_links": {
    "self": {
        "href": "http://127.0.0.1:8080/api/posts/5/likedBy"
    }
}
};