export default {
    "name": "NameTest",
    "surname": "SurnameTest",
    "email": "mail2",
    "password": "pass2",
    "username": "username2",
    "photo": "photo2",
    "likedComments": [{
        "sentence": "messageCommentRoot3",
        "_links": {
            "post": {
                "href": "http://localhost:8080/api/posts/7"
            },
            "comments": {
                "href": "http://localhost:8080/api/commentLeaves/19"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/1"
            }, {
                "href": "http://localhost:8080/api/users/2"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/3"
            }
        }
    }, {
        "sentence": "messageCommentLeaf3",
        "comments": {
            "sentence": "messageCommentRoot3"
        },
        "_links": {
            "commentRoot": {
                "href": "http://localhost:8080/api/commentRoots/16"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/2"
            }, {
                "href": "http://localhost:8080/api/users/3"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/1"
            }
        }
    }, {
        "sentence": "messageCommentRoot1",
        "_links": {
            "post": {
                "href": "http://localhost:8080/api/posts/5"
            },
            "comments": {
                "href": "http://localhost:8080/api/commentLeaves/17"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/2"
            }, {
                "href": "http://localhost:8080/api/users/3"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/1"
            }
        }
    }, {
        "sentence": "messageCommentLeaf1",
        "comments": {
            "sentence": "messageCommentRoot1"
        },
        "_links": {
            "commentRoot": {
                "href": "http://localhost:8080/api/commentRoots/14"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/1"
            }, {
                "href": "http://localhost:8080/api/users/2"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/3"
            }
        }
    }],
    "myComments": [{
        "sentence": "messageCommentRoot2",
        "_links": {
            "post": {
                "href": "http://localhost:8080/api/posts/6"
            },
            "comments": {
                "href": "http://localhost:8080/api/commentLeaves/18"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/1"
            }, {
                "href": "http://localhost:8080/api/users/3"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/2"
            }
        }
    }, {
        "sentence": "messageCommentLeaf2",
        "comments": {
            "sentence": "messageCommentRoot2"
        },
        "_links": {
            "commentRoot": {
                "href": "http://localhost:8080/api/commentRoots/15"
            },
            "likedBy": [{
                "href": "http://localhost:8080/api/users/3"
            }, {
                "href": "http://localhost:8080/api/users/1"
            }],
            "author": {
                "href": "http://localhost:8080/api/users/2"
            }
        }
    }],
    "_links": {
        "self": {
            "href": "http://localhost:8080/api/users/2"
        },
        "user": {
            "href": "http://localhost:8080/api/users/2"
        },
        "likedPost": {
            "href": "http://localhost:8080/api/users/2/likedPost"
        },
        "friends": {
            "href": "http://localhost:8080/api/users/2/friends"
        },
        "myGroups": {
            "href": "http://localhost:8080/api/users/2/myGroups"
        },
        "posts": {
            "href": "http://localhost:8080/api/users/2/posts"
        }
    }
};