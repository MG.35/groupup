export default {
    "_embedded": {
        "commentRoots": [
            {
                "sentence": "messageCommentRoot1",
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/api/commentRoots/16"
                    },
                    "commentRoot": {
                        "href": "http://localhost:8080/api/commentRoots/16"
                    },
                    "likedBy": {
                        "href": "http://localhost:8080/api/commentRoots/16/likedBy"
                    },
                    "post": {
                        "href": "http://localhost:8080/api/commentRoots/16/post"
                    },
                    "comments": {
                        "href": "http://localhost:8080/api/commentRoots/16/comments"
                    },
                    "author": {
                        "href": "http://localhost:8080/api/commentRoots/16/author"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/api/posts/5/comments"
        }
    }
}