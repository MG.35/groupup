export default {
    id: 5,
    message: "messagePost1",
    date: "2022-04-02T16:40:06.800+00:00",
    _links: {
        self: {
            href: "http://localhost:8080/api/posts/5"
        },
        post: {
            href: "http://localhost:8080/api/posts/5"
        },
        comments: {
            href: "http://localhost:8080/api/posts/5/comments"
        },
        myGroup: {
            href: "http://localhost:8080/api/posts/5/myGroup"
        },
        author: {
            href: "http://localhost:8080/api/posts/5/author"
        },
        likedBy: {
            href: "http://localhost:8080/api/posts/5/likedBy"
        },
        savedPosts: {
            href: "http://localhost:8080/api/posts/5/savedPosts"
        }
    }
};