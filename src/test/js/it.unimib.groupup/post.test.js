import {queryByAttribute, render, waitFor, fireEvent, getByTestId} from "@testing-library/react";
import React from "react";
import Post from "../../../main/js/shared/post/post";
import {createServer, Model} from "miragejs";
import '@testing-library/jest-dom';
import getApiPosts5 from "./mock-calls/gets/getApiPosts5";
import getApiPosts5Author from "./mock-calls/gets/getApiPosts5Author";
import getApiPosts5likedBy from "./mock-calls/gets/getApiPost5LikedBy";
import UserContext from "../../../main/js/shared/user-context/user_context";
import getApiPost5Comments from "./mock-calls/gets/getApiPost5Comments";


let server;
const post = getApiPosts5;
const post5Comments = getApiPost5Comments
const commentsAuthor = getApiPosts5Author

const user = {
    id: 1,
    name: "Willis",
    surname: "Lyon",
    email: "w.lyon@domain.com",
    password: "password",
    username: "frances",
    photo: "/user1.jpg",
    myLabels: [{
        "label" : "My Label",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/api/labels/22"
            },
            "myLabel" : {
                "href" : "http://localhost:8080/api/labels/22"
            },
            "user" : {
                "href" : "http://localhost:8080/api/labels/22/user"
            },
            "savedPosts" : {
                "href" : "http://localhost:8080/api/labels/22/savedPosts"
            }
        }
    },{
        "label" : "My Second Label",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/api/labels/23"
            },
            "myLabel" : {
                "href" : "http://localhost:8080/api/labels/23"
            },
            "user" : {
                "href" : "http://localhost:8080/api/labels/23/user"
            },
            "savedPosts" : {
                "href" : "http://localhost:8080/api/labels/23/savedPosts"
            }
        }
    }]
}

function postRender(label){
    return <UserContext.Provider value={user}>
                <Post post={post} label={label}/>
            </UserContext.Provider>
}

beforeEach(() => {
    server = createServer({
        models: {
            savedPosts: Model
        },
        routes() {
            this.get("http://localhost:8080/api/commentRoots/*", () => commentsAuthor);
            this.get("http://localhost:8080/api/posts/5/comments", () => post5Comments)
            this.get("http://localhost:8080/api/posts/5/savedPosts", () => {
                    return {
                      "_embedded" : {
                        "savedPosts" : [ {
                          "_links" : {
                            "self" : {
                              "href" : "http://localhost:8080/api/savedPosts/24"
                            },
                            "savedPost" : {
                              "href" : "http://localhost:8080/api/savedPosts/24"
                            },
                            "user" : {
                              "href" : "http://localhost:8080/api/savedPosts/24/user"
                            },
                            "label" : {
                              "href" : "http://localhost:8080/api/savedPosts/24/label"
                            },
                            "post" : {
                              "href" : "http://localhost:8080/api/savedPosts/24/post"
                            }
                          }
                        } ]
                      },
                      "_links" : {
                        "self" : {
                          "href" : "http://localhost:8080/api/posts/5/savedPosts"
                        }
                      }
                    }

            });
            this.delete("http://localhost:8080/api/savedPosts/24", () => {
                schema.savedPosts = [];
            });
            this.get("http://localhost:8080/api/savedPosts/24/label", () => {
                return {
                    "label" : "My Label",
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "myLabel" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "savedPosts" : {
                            "href" : "http://localhost:8080/api/labels/22/savedPosts"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/labels/22/user"
                        }
                    }
                }
            });
            this.get("http://localhost:8080/api/savedPosts/24/label", () => {
                return {
                    "label" : "My Label",
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "myLabel" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "savedPosts" : {
                            "href" : "http://localhost:8080/api/labels/22/savedPosts"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/labels/22/user"
                        }
                    }
                }
            });
            this.get("/api/labels/search/*", () => {
                return {
                    "label" : "Motorsport",
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "myLabel" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/labels/22/user"
                        },
                        "savedPosts" : {
                            "href" : "http://localhost:8080/api/labels/22/savedPosts"
                        }
                    }
                }
            });
            this.get("/api/savedPosts/search/*", () => {
                return {
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "savedPost" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/user"
                        },
                        "post" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/post"
                        },
                        "label" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/label"
                        }
                    }
                }
            });
            this.get("/api/myGroups/:id/users", () => {
                return {"_embedded": {"users": []}}
            });
            this.get("http://localhost:8080/api/posts/5/author", () => {
                return getApiPosts5Author
            });
            this.get("http://localhost:8080/api/posts/5/likedBy", () => {
                return getApiPosts5likedBy
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

describe("Post component tests:", () => {

        it("Post's information is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(postRender());

            waitFor(() => {
                expect(getById(page.container, 'post-' + post.id) == null).toBe(false);
                expect(getById(page.container, 'postDate-' + post.id)).toHaveTextContent("02-04-2022 06:40");
                expect(getById(page.container, 'postMessage-' + post.id)).toHaveTextContent("messagePost1");
            });
        });

        it("Post's author is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(postRender());
    
            waitFor(() => {
                expect(getById(page.container, 'authorName-' + post.id)).toHaveTextContent("name2 surname2");
                expect(getById(page.container, 'authorUsername-' + post.id)).toHaveTextContent("@username2");
            });
        });

        it("Post's like button is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(postRender());
            const nLikes = "2"

            waitFor(() => {
                expect(getById(page.container, 'like-' + post.id)).toHaveTextContent(nLikes);
            });
        });

        it("Show comments with collapsing is working", () => {
            const {container} = render(postRender());
            expect(getByTestId(container, 'toggleComments-' + post.id))
                .toHaveTextContent("show comments...");
            fireEvent.click(getByTestId(container, 'toggleComments-' + post.id).childNodes[0]);
            return waitFor(()=>
                expect(getByTestId(container,'collapseComments-' + post.id).childNodes == null).toBe(false))
        });

        it("Post's remove from label button is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(postRender("My Label"));
            fireEvent.click(getById(page.container, `labelButton-${post.id}`));
            waitFor(() => {
                expect(getById(page.container, 'label-modal')).toBeVisible();
            });
            waitFor(() => {expect(getById(page.container, `remove-post-label`) == null).toBe(false)});
        })

        it("Post is removed from label when remove from label button is clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(postRender("My Label"));
            waitFor(() => {
                fireEvent.click(getById(page.container, `labelButton-${post.id}`));
                fireEvent.click(getById(page.container, `remove-post-label`));
                waitFor(() => {expect(server.db.savedPosts.length).toBe(0)});
            })
        })

        it("Label is added to a post", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const getByCss = queryByAttribute.bind(null, 'css');
            const page = render(postRender("My Label"));
            waitFor(() => {
                fireEvent.click(getById(page.container, `labelButton-${post.id}`));
                fireEvent.click(getByCss(page.container, `.css-tj5bde-Svg`));
                fireEvent.click(getById(page.container, `react-select-2-option-0`));
                fireEvent.click(getById(page.container, `save-label`));
                waitFor(() => {expect(server.db.savedPosts[0].label).toBe("My Second Label")});
            })
        })
    }
)