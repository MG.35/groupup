import React from "react";
import {queryByAttribute, render, waitFor, fireEvent} from "@testing-library/react";
import JoinGroupButton from "../../../main/js/group/join_group_button";
import {createServer, Model} from "miragejs";

let server;


beforeEach(() => {
    server = createServer({
        models: {
            users: Model
        },
        routes() {
            this.get("/api/myGroups/1/users", (schema) => {
                return schema.db.users[0];
            });
            this.put("/api/myGroups/1/users", (schema, request) => {
                const new_friends = request.requestBody.split("\n");
                schema.users.create({
                    "_embedded": {
                        "users": [
                            {
                                "_links": {
                                    "self": {
                                        "href": new_friends[0]
                                    }
                                }
                            }
                        ]
                    }
                });
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});


describe("Join group button test", () => {

        it("Should render join group button", () => {
                const getById = queryByAttribute.bind(null, 'id');
                server.db.users.insert({
                    "_embedded": {
                        "users": []
                    }
                })
                const page = render(
                    <JoinGroupButton userid={1} groupid={1}/>
                );
                expect(getById(page.container, 'join-group') == null).toBe(false);
            }
        )

        it("Should render leave group button", () => {
            const getById = queryByAttribute.bind(null, 'id');
            server.db.users.insert({
                "_embedded": {
                    "users": [
                        {
                            "_links": {
                                "self": {
                                    "href": "http://localhost/api/users/1"
                                }
                            }
                        }
                    ]
                }
            });
            const page = render(
                <JoinGroupButton userid={1} groupid={1}/>
            );
            waitFor(() => expect(getById(page.container, 'leave-group') == null).toBe(false));

        });

        it("Should join group when button is clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(
                <JoinGroupButton userid={1} groupid={1}/>
            );
            fireEvent.click(getById(page.container, "join-group"));
            waitFor(() => expect(server.db.users[0]["_embedded"]["users"][0] == null).toBe(false));
        })

    }
)
