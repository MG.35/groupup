import React from "react";
import Board from "../../../main/js/board/board";
import UserContext from "../../../main/js/shared/user-context/user_context"
import '@testing-library/jest-dom';
import {queryByAttribute, render, waitFor} from "@testing-library/react";
import {createServer} from "miragejs";
import '@testing-library/jest-dom';
import getApiPosts8Author from "./mock-calls/gets/getApiPosts8Author";
import getApiPostsSearchBoardPosts from "./mock-calls/gets/getApiPostsSearchBoardPosts";


const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

let server;

beforeEach(() => {
    server = createServer({
        routes() {

            this.get("http://localhost:8080/api/posts/8/author", () => {
                return getApiPosts8Author
            });


            this.get("/api/posts/search/board-posts", (schema, request) => {

                expect(parseInt(request.queryParams.userId) === parseInt(user.id)).toBe(true);

                return getApiPostsSearchBoardPosts
            })
        },
    })
})
describe("Board component test:", () => {
    it("Board message is correct", () => {
            const page = render(
                <UserContext.Provider value={user}><Board/></UserContext.Provider>
            )

            const getById = queryByAttribute.bind(null, 'id');
            waitFor(() => expect(getById(page.container, "postMessage-8")).toHaveTextContent("messagePost4"));

        }
    );
})