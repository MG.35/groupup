import React from "react";
import {queryByAttribute, render, waitFor} from "@testing-library/react";
import {createServer, Model} from "miragejs";
import {MemoryRouter, Route, Routes} from "react-router-dom";
import '@testing-library/jest-dom';
import UserContext from "../../../main/js/shared/user-context/user_context";
import getApiMyGroups4 from "./mock-calls/gets/getApiMyGroups4";
import getApiMyGroups4Posts from "./mock-calls/gets/getApiMyGroups4Posts";
import getApiPosts5Author from "./mock-calls/gets/getApiPosts5Author";
import GroupList from "../../../main/js/group/group_list";
import getApiUsers1MyGroups from "./mock-calls/gets/getApiUsers1MyGroups";

let server;
const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

beforeEach(() => {
    server = createServer({
        models: {
            groups: Model
        },
        routes() {
            this.get("/api/users/1/myGroups", () => {
                return getApiUsers1MyGroups
            });
            this.get("http://localhost:8080/api/myGroups/4/posts", () => {
                return getApiMyGroups4Posts
            });
            this.get("http://localhost:8080/api/posts/5/author", () => {
                return getApiPosts5Author
            });
            this.get("/api/myGroups/:id/users", () => {
                return {"_embedded": {"users": []}}
            });
            this.get("/api/myGroups/4", () => {
                return getApiMyGroups4
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

describe("GroupList component tests:",  () => {
    it("First group of the list is correct",  () => {
           const page = render(
               <MemoryRouter initialEntries={["/"]}>
                   <Routes>
                       <Route path={"/"}
                              element={<UserContext.Provider value={user}><GroupList /></UserContext.Provider>}/>
                   </Routes>
               </MemoryRouter>
           )
           const getById = queryByAttribute.bind(null, 'id');
           waitFor(() => {
               expect(getById(page.container, "group-name-4")).toHaveTextContent("name");
               expect(getById(page.container, "group-description-4")).toHaveTextContent("description");

           });
        }
    );
})