import React from "react";
import {queryByAttribute, render, fireEvent} from "@testing-library/react";
import { createServer, Model } from "miragejs"
import LabelCreate from "../../../main/js/labels/label_create";
import UserContext from "../../../main/js/shared/user-context/user_context";
import '@testing-library/jest-dom';
import {MemoryRouter, Routes, Route} from "react-router-dom";

let server;

const user = {
    id: 1,
    name : "name1",
    surname : "surname1",
    email : "mail1",
    password : "pass1",
    username : "username1",
    photo : "photo1",
}

beforeEach(() => {
    server = createServer({
        models: {
            gs: Model
        },
        routes() {
            this.post("/api/labels", (schema, request) => {
                let attrs = JSON.parse(request.requestBody)
                schema.gs.create({label: attrs.label, user: attrs.user});
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

function renderUserWithContext() {
    return(
        <UserContext.Provider value={user}>
            <MemoryRouter initialEntries={["/"]}>
                <Routes>
                    <Route path={"/"} element={<LabelCreate />}/>
                </Routes>
            </MemoryRouter>
        </UserContext.Provider>
    )
}

describe("New Label creation component tests:", () => {

        it("Label is displayed", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderUserWithContext());
                expect(getById(page.container, 'label_id') == null).toBe(false);
            }
        )

        it("Create label button should be clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(renderUserWithContext());
            const test_label = "TEST_NAME"

            fireEvent.change(getById(page.container, "label_id"), {
                target: {value: test_label},
            });

            fireEvent.click(getById(page.container, "button-c"));
            expect(server.db.gs[0].label).toBe(test_label);
            expect(server.db.gs[0].user).toBe("/api/users/" + user.id);
        })

    }
)

