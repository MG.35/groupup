import React from "react";
import {queryByAttribute, render, waitFor} from "@testing-library/react";
import {createServer, Model} from "miragejs";
import {MemoryRouter, Route, Routes} from "react-router-dom";
import Group from "../../../main/js/group/group";
import '@testing-library/jest-dom';
import UserContext from "../../../main/js/shared/user-context/user_context";
import getApiMyGroups4 from "./mock-calls/gets/getApiMyGroups4";
import getApiMyGroups4Posts from "./mock-calls/gets/getApiMyGroups4Posts";
import getApiPosts5Author from "./mock-calls/gets/getApiPosts5Author";

let server;
const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

beforeEach(() => {
    server = createServer({
        models: {
            groups: Model
        },
        routes() {
            this.get("/api/myGroups/:id/users", () => {
                return {"_embedded": {"users": []}}
            });

            this.get("api/myGroups/4", () => {
                return getApiMyGroups4
            });
            this.get("http://localhost:8080/api/myGroups/4/posts", () => {
                return getApiMyGroups4Posts
            });
            this.get("http://localhost:8080/api/posts/5/author", () => {
                return getApiPosts5Author
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

describe("Group component tests:", () => {
    it("Post message is correct", () => {

            const page = render(
                <MemoryRouter initialEntries={["/4"]}>
                    <Routes>
                        <Route path={"/:id"}
                               element={<UserContext.Provider value={user}><Group/></UserContext.Provider>}/>
                    </Routes>
                </MemoryRouter>
            )

            const getById = queryByAttribute.bind(null, 'id');
            waitFor(() => {
                expect(getById(page.container, "postMessage-5")).toHaveTextContent("messagePost1");

            });

        }
    );
    it("Group information is correct", () => {

        const page = render(
            <MemoryRouter initialEntries={["/4"]}>
                <Routes>
                    <Route path={"/:id"}
                           element={<UserContext.Provider value={user}><Group/></UserContext.Provider>}/>
                </Routes>
            </MemoryRouter>
        )
        const getById = queryByAttribute.bind(null, 'id');
        waitFor(() => {
            expect(getById(page.container, "photo")).toHaveTextContent("photo");
            expect(getById(page.container, "name")).toHaveTextContent("name");
        });
    })
})