import {fireEvent, queryByAttribute, render, waitFor} from "@testing-library/react";
import React from "react";
import {createServer, Model} from "miragejs";
import '@testing-library/jest-dom';
import Posts from "../../../main/js/shared/post/posts";
import getApiPosts5 from "./mock-calls/gets/getApiPosts5";
import getApiPosts5Author from "./mock-calls/gets/getApiPosts5Author";
import getApiPosts5likedBy from "./mock-calls/gets/getApiPost5LikedBy";
import UserContext from "../../../main/js/shared/user-context/user_context";


let posts = [
    getApiPosts5
]

let server

beforeEach(() => {
    server = createServer({
        models: {
            post: Model,
            user: Model,
            savedPosts: Model
        },
        routes() {
            this.get("http://localhost:8080/api/posts/5/author", () => {
                return getApiPosts5Author
            });
            this.get("http://localhost:8080/api/posts/5/likedBy", () => {
                return getApiPosts5likedBy
            });
            this.get("/api/labels/search/*", () => {
                return {
                    "label" : "Motorsport",
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "myLabel" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/labels/22/user"
                        },
                        "savedPosts" : {
                            "href" : "http://localhost:8080/api/labels/22/savedPosts"
                        }
                    }
                }
            });
            this.get("/api/savedPosts/search/*", () => {
                return {
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "savedPost" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/user"
                        },
                        "post" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/post"
                        },
                        "label" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/label"
                        }
                    }
                }
            });
            this.delete("http://localhost:8080/api/posts/5", () => {
                posts = []
            });
        }
    })
});

const user = {
    id: 2,
    name: "name2",
    surname: "surname2",
    email: "mail2",
    password: "pass2",
    username: "username2",
    photo: "photo2",
    myLabels: []
}

function renderComponentWithContext() {
    return (
        <UserContext.Provider value={user}>
            <Posts posts={posts}/>
        </UserContext.Provider>
    )
}

afterEach(() => {
    server.shutdown();
});


describe("Remove post component tests:", () => {

        it("Remove a post", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(renderComponentWithContext())
            const waitForRender = waitFor(() => expect(getById(page.container, 'remove-post-5') == null).toBe(false));
            return waitForRender.then(() => {
                expect(fireEvent.click(getById(page.container, "remove-post-5"))).toBeTruthy()
                expect(posts.length).toBe(0)
            })
        });
    }
)