import React from "react";
import {queryByAttribute, render} from "@testing-library/react";
import {Information} from "../../../main/js/shared/information/information";
import '@testing-library/jest-dom';

const loggedUser = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "./static/user1.jpg",
}

const user = {
    id: 2,
    name: "name2",
    surname: "surname2",
    email: "mail2",
    password: "pass2",
    username: "username2",
    photo: "./static/user2.jpg",
}

const group = {
    id: 3,
    name: "name3",
    description: "description3",
    photo: "./static/group.jpg",
}

function renderUserInformation() {
    return (
            <Information name={user.name + " " + user.surname} username={user.username} userid={loggedUser.id} currentuserid={user.id}/>
    )
}

function renderGroupInformation() {
    return (
        <Information name={group.name} groupid={group.id} userid={loggedUser.id}/>
    )
}

describe("Information component tests:", () => {


    it("User's information is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(renderUserInformation());
            expect(getById(page.container, 'information') == null).toBe(false);

            expect(getById(page.container, 'name') == null).toBe(false);
            expect(getById(page.container, 'name')).toHaveTextContent("name2");

            expect(getById(page.container, 'username') == null).toBe(false);
            expect(getById(page.container, 'username')).toHaveTextContent("username2");

            expect(getById(page.container, 'add-friend') == null && getById(page.container, 'remove-friend') == null).toBe(false);
        }
    )

    it("Group's information is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(renderGroupInformation());
            expect(getById(page.container, 'information') == null).toBe(false);

            expect(getById(page.container, 'name') == null).toBe(false);
            expect(getById(page.container, 'name')).toHaveTextContent("name3");

            expect(getById(page.container, 'join-group') == null && getById(page.container, 'leave-group') == null).toBe(false);
        }
    )


    }
)