import React from "react";
import {queryByAttribute, render, waitFor, fireEvent} from "@testing-library/react";
import AddFriendButton from "../../../main/js/profile/add_friend_button";
import {createServer, Model} from "miragejs";

let server;


beforeEach(() => {
    server = createServer({
        models: {
            users: Model
        },
        routes() {
            this.get("/api/users/1/friends", (schema) => {
                return schema.db.users[0];
            });
            this.put("/api/users/1/friends", (schema, request) => {
                const new_friends = request.requestBody.split("\n");
                schema.users.create({
                    "_embedded": {
                        "users": [
                            {
                                "_links": {
                                    "self": {
                                        "href": new_friends[0]
                                    }
                                }
                            }
                        ]
                    }
                });
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});


describe("Add friend button test", () => {

        it("Should render add friend button", () => {
                const getById = queryByAttribute.bind(null, 'id');
                server.db.users.insert({
                    "_embedded": {
                        "users": []
                    }
                })
                const page = render(
                    <AddFriendButton userid={1} currentuserid={3}/>
                );
                expect(getById(page.container, 'add-friend') == null).toBe(false)
            }
        )

        it("Should render no button", () => {
            const getById = queryByAttribute.bind(null, 'id');
            server.db.users.insert({
                "_embedded": {
                    "users": []
                }
            })
            const page = render(
                <AddFriendButton userid={1} currentuserid={1}/>
            );
            expect(getById(page.container, 'add-friend') == null).toBe(true)
        })

        it("Should render remove friend button", () => {
            const getById = queryByAttribute.bind(null, 'id');
            server.db.users.insert({
                "_embedded": {
                    "users": [
                        {
                            "_links": {
                                "self": {
                                    "href": "http://localhost/api/users/2"
                                }
                            }
                        }
                    ]
                }
            });
            const page = render(
                <AddFriendButton userid={1} currentuserid={2}/>
            );
            waitFor(() => expect(getById(page.container, 'remove-friend') == null).toBe(false));

        });

        it("Should add friend when button is clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(
                <AddFriendButton userid={1} currentuserid={2}/>
            )
            fireEvent.click(getById(page.container, "add-friend"));
            expect(server.db.users[0]["_embedded"]["users"][0] == null).toBe(false)
        })

    }
)