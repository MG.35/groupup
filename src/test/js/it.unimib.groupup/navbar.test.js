import React from "react";
import {queryByAttribute, render} from "@testing-library/react";
import Navbar from "../../../main/js/navigation/navbar/navbar";
import UserContext from "../../../main/js/shared/user-context/user_context";

const user = {
        id: 1,
        name : "name1",
        surname : "surname1",
        email : "mail1",
        password : "pass1",
        username : "username1",
        photo : "photo1",
}

function renderUserWithContext() {
        return(
            <UserContext.Provider value={user}>
                    <Navbar />
            </UserContext.Provider>
        )
}

describe("Navbar component tests:", () => {
        it("Check that the various elements are present", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderUserWithContext());
                expect(getById(page.container, 'navbar-brand') == null).toBe(false);
                // not implemented yet
                // expect(getById(page.container, 'navbar-search') == null).toBe(false);
                expect(getById(page.container, 'navbar-board') == null).toBe(false);
                // not implemented yet
                // expect(getById(page.container, 'navbar-group') == null).toBe(false);
                expect(getById(page.container, 'navbar-profile') == null).toBe(false);

                expect(getById(page.container, 'navbar-logout') == null).toBe(false);

            }
        )
    }
)
