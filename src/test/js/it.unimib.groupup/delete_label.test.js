import React from "react";
import {queryByAttribute, render, fireEvent} from "@testing-library/react";
import SavedLabel from "../../../main/js/saved_posts_with_labels/saved_label";
import {BrowserRouter} from "react-router-dom";
import {createServer, Model} from "miragejs"

const props = {
    key: "1",
    label: {
        "label": "Motorsport",
        "_links": {
            "self": {
                "href": "http://localhost:8080/api/labels/22"
            },
            "myLabel": {
                "href": "http://localhost:8080/api/labels/22"
            },
            "user": {
                "href": "http://localhost:8080/api/labels/22/user"
            },
            "savedPosts": {
                "href": "http://localhost:8080/api/labels/22/savedPosts"
            }
        }
    }
}

let server;

beforeEach(() => {
    server = createServer({
        models: {
            labels: Model
        },
        routes() {
            this.delete("http://localhost:8080/api/labels/22", (schema) => {
                const label = schema.labels.first();
                label.destroy()
            });
            this.get("http://localhost:8080/api/labels/22/savedPosts", () => {
                return {
                    "_embedded": {
                        "savedPosts": []
                    }
                }
            })
        }
    });
    server.db.loadData({
        labels: [props.label]
    })
});

afterEach(() => {
    server.shutdown();
});


function renderSavedLabel() {
    return (
        <BrowserRouter>
            <SavedLabel key={props.key} label={props.label}/>
        </BrowserRouter>
    )
}

describe("SavedLabel component tests:", () => {
    it("the delete label button is rendered", () => {
        const getById = queryByAttribute.bind(null, 'id');
        const page = render(renderSavedLabel());
        expect(getById(page.container, "delete-label") == null).toBe(false);
    })
    it("the delete label button should delete label when clicked", () => {
        const getById = queryByAttribute.bind(null, 'id');
        const page = render(renderSavedLabel());

        fireEvent.click(getById(page.container, "delete-label"));
        expect(server.db.labels.length).toBe(0);

    })
});
