import {queryByAttribute, render} from "@testing-library/react";
import React from "react";
import {createServer, Model} from "miragejs";
import '@testing-library/jest-dom';
import Posts from "../../../main/js/shared/post/posts";
import getApiPosts5 from "./mock-calls/gets/getApiPosts5";
import getApiPosts5Author from "./mock-calls/gets/getApiPosts5Author";
import getApiPosts5likedBy from "./mock-calls/gets/getApiPost5LikedBy";
import Post from "../../../main/js/shared/post/post";
import UserContext from "../../../main/js/shared/user-context/user_context";

let server;
const posts = [
    getApiPosts5
]

const user = {
    id: 1,
    name: "Willis",
    surname: "Lyon",
    email: "w.lyon@domain.com",
    password: "password",
    username: "frances",
    photo: "/user1.jpg",
    myLabels: []
}

beforeEach(() => {
    server = createServer({
        models: {
            groups: Model
        },
        routes() {
            this.get("/api/myGroups/:id/users", () => {
                return {"_embedded": {"users": []}}
            });
            this.get("/api/labels/search/*", () => {
                return {
                    "label" : "Motorsport",
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "myLabel" : {
                            "href" : "http://localhost:8080/api/labels/22"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/labels/22/user"
                        },
                        "savedPosts" : {
                            "href" : "http://localhost:8080/api/labels/22/savedPosts"
                        }
                    }
                }
            });
            this.get("/api/savedPosts/search/*", () => {
                return {
                    "_links" : {
                        "self" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "savedPost" : {
                            "href" : "http://localhost:8080/api/savedPosts/23"
                        },
                        "user" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/user"
                        },
                        "post" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/post"
                        },
                        "label" : {
                            "href" : "http://localhost:8080/api/savedPosts/23/label"
                        }
                    }
                }
            });
            this.get("http://localhost:8080/api/posts/5/author", () => {
                return getApiPosts5Author
            });
            this.get("http://localhost:8080/api/posts/5/likedBy", () => {
                return getApiPosts5likedBy
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

describe("Posts component tests:", () => {

        it("Post list is displayed", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(<UserContext.Provider value={user}>
                                <Posts posts={posts}/>
                            </UserContext.Provider>);
            expect(getById(page.container, 'posts') == null).toBe(false);
            expect(getById(page.container, 'post-' + posts[0].id) == null).toBe(false);
        });

    }
)