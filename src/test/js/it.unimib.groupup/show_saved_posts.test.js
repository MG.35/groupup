import React from "react";
import {queryByAttribute, render, waitFor} from "@testing-library/react";
import UserContext from "../../../main/js/shared/user-context/user_context";
import SavedPosts from "../../../main/js/saved_posts_with_labels/saved_posts";
import {createServer, Model} from "miragejs";
import getApiUsers1MyLabels from "./mock-calls/gets/getApiUsers1MyLabels";
import getApiLabels22SavedPosts from "./mock-calls/gets/getApiLabels22SavedPosts";
import getApiSavedPost24Post from "./mock-calls/gets/getApiSavedPost24Post";
import {MemoryRouter, Route, Routes} from "react-router-dom";

let server

let gets = 0

beforeEach(() => {
    server = createServer({
        models: {
            post: Model,
            user: Model
        },
        routes() {
            this.get("/api/users/1/myLabels", () => {
                gets = gets + 1
                return getApiUsers1MyLabels
            });

            this.get("http://localhost:8080/api/labels/22/savedPosts", () => {
                gets = gets + 1
                return getApiLabels22SavedPosts
            })
            this.get("http://localhost:8080/api/savedPosts/24/post", () => {
                return getApiSavedPost24Post
            })

        }
    })
});

afterEach(() => {
    server.shutdown();
});

const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

function renderComponentWithContext() {
    return (
    <MemoryRouter initialEntries={["/Motorsport"]}>
        <Routes>
            <Route path={"/:label"}
                   element={<UserContext.Provider value={user}><SavedPosts/></UserContext.Provider>}/>
        </Routes>
    </MemoryRouter>
    )
}

describe("SavedPosts component tests:", () => {


        it("the page of saved posts is rendered", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderComponentWithContext());
                expect(getById(page.container, 'posts-label') == null).toBe(false);
                return waitFor(() => {
                    expect(gets).toBe(2)
                });

            }
        )


    }
)