import React from "react";
import {queryByAttribute, render} from "@testing-library/react";
import Profile from "../../../main/js/profile/profile";
import UserContext from "../../../main/js/shared/user-context/user_context";

const user = {
    id: 1,
    name: "name1",
    surname: "surname1",
    email: "mail1",
    password: "pass1",
    username: "username1",
    photo: "photo1",
}

function renderUserWithContext() {
    return (
        <UserContext.Provider value={user}>
            <Profile/>
        </UserContext.Provider>
    )
}

describe("Profile component tests:", () => {


        it("Personal page posts are displayed", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(renderUserWithContext());
                expect(getById(page.container, 'profile') == null).toBe(false);
                expect(getById(page.container, 'personal-posts') == null).toBe(false);
                expect(getById(page.container, 'information') == null).toBe(false);
            }
        )


    }
)