import React from "react";
import {queryByAttribute, render, fireEvent} from "@testing-library/react";
import NewPost from "../../../main/js/new_post/new_post_group";
import { createServer, Model } from "miragejs"

let server;

beforeEach(() => {
    server = createServer({
        models: {
            posts: Model
        },

        routes() {
            this.post("/api/posts", (schema, request) => {
                let attrs = JSON.parse(request.requestBody)
                schema.posts.create({message: attrs.message, author: attrs.author, myGroup: attrs.myGroup});
            });
        }
    })
});

afterEach(() => {
    server.shutdown();
});

describe("New group post component tests:", () => {

        it("Message text area input is displayed", () => {
                const getById = queryByAttribute.bind(null, 'id');
                const page = render(<NewPost id={1} group_id={1} />);
                expect(getById(page.container, 'message-input') == null).toBe(false);
            }
        )

        it("Add group post button should be clicked", () => {
            const getById = queryByAttribute.bind(null, 'id');
            const page = render(<NewPost id={1} group_id={1}/>);

            fireEvent.change(getById(page.container, "message-input"), {
                target: {value: 'New group post text'},
            });

            fireEvent.click(getById(page.container, "add-post"));
            expect(server.db.posts[0].message).toBe("New group post text");
        })

    }
)