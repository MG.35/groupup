package it.unimib.groupup.unit;

import it.unimib.groupup.model.MyGroup;
import it.unimib.groupup.model.Post;
import it.unimib.groupup.model.User;
import it.unimib.groupup.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class PostUTest {

    @Autowired
    CommentLeafRepository commentLeafRepository;
    @Autowired
    CommentRootRepository commentRootRepository;
    @Autowired
    MyGroupRepository myGroupRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void testPostWithoutOtherEntities() {
        Post post = new Post();
        post.setId(44);
        post.setMessage("test");
        postRepository.save(post);

        // check if the post is instantiated correctly
        String toStringExpected = "Post{id=44, message='test'}";
        String toStringActual = post.toString();
        assert (toStringActual.equals(toStringExpected));

        // save the post
        Post savedPost = postRepository.save(post);

        // check if the post is saved correctly
        Optional<Post> foundPost = postRepository.findById(savedPost.getId());
        assert (foundPost.isPresent());
        String toStringExpectedSaved = "Post{id=" + savedPost.getId() + ", message='test'}";
        String toStringActualSaved = foundPost.get().toString();
        assert (toStringActualSaved.equals(toStringExpectedSaved));

        // delete the post
        postRepository.delete(savedPost);

        // check if the post is deleted correctly
        Optional<Post> postDeleted = postRepository.findById(savedPost.getId());
        assert (postDeleted.isEmpty());
    }

    @Test
    @Transactional
    void testPostWithOtherEntities() {
        // instantiate the post
        Post post = new Post();
        post.setMessage("test");

        // save the post
        Post savedPost = postRepository.save(post);

        // instantiate the user
        User user = new User();
        user.setUsername("test");
        user.setPassword("test");
        List<Post> posts = new java.util.ArrayList<Post>();
        posts.add(post);
        user.setPosts(posts);

        // instantiate the group
        MyGroup group = new MyGroup();
        group.setName("test");
        group.setPosts(posts);

        // save the user
        userRepository.save(user);

        // save the group
        myGroupRepository.save(group);

        // save the post
        postRepository.save(post);

        // check if the post is saved correctly
        Optional<Post> foundPost = postRepository.findById(savedPost.getId());
        assert (foundPost.isPresent());

        // check that user has the post
        Optional<User> foundUser = userRepository.findById(user.getId());
        assert (foundUser.isPresent());
        assert (foundUser.get().getPosts().contains(foundPost.get()));

        // check that group has the post
        Optional<MyGroup> foundGroup = myGroupRepository.findById(group.getId());
        assert (foundGroup.isPresent());
        assert (foundGroup.get().getPosts().contains(foundPost.get()));

    }
}
