package it.unimib.groupup.unit;

import it.unimib.groupup.model.MyLabel;
import it.unimib.groupup.model.Post;
import it.unimib.groupup.model.SavedPost;
import it.unimib.groupup.model.User;
import it.unimib.groupup.repositories.MyLabelsRepository;
import it.unimib.groupup.repositories.PostRepository;
import it.unimib.groupup.repositories.SavedPostRepository;
import it.unimib.groupup.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class SavedPostsUTest {

    @Autowired
    private SavedPostRepository savedPostRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private MyLabelsRepository myLabelsRepository;

    @Test
    void testPostWithoutOtherEntities() {
        SavedPost savedPost = new SavedPost();

        // add a label

        // save the saved post
        this.savedPostRepository.save(savedPost);

        // check if the saved post is in the database
        assert(this.savedPostRepository.findById(savedPost.getId()).isPresent());


    }

    @Test
    @Transactional
    void testCommentLeadWithOtherEntities() {
        // create a saved post
        SavedPost savedPost = new SavedPost();


        // create a user and a post
        User user = this.userRepository.save(new User());
        Post post = this.postRepository.save(new Post());

        // create a new label for the user
        MyLabel label = new MyLabel();
        label.setUser(user);
        label.setLabel("label");

        // add a user
        savedPost.setUser(user);

        // set the label
        savedPost.setLabel(label);

        // add a post
        savedPost.setPost(post);

        // save the saved post
        this.savedPostRepository.save(savedPost);

        // check if the saved post is in the database
        assert(this.savedPostRepository.findById(savedPost.getId()).isPresent());

        // assert the label
        assert(this.savedPostRepository.findById(savedPost.getId()).get().getLabel().getLabel().equals("label"));

        // assert the user
        assert(this.savedPostRepository.findById(savedPost.getId()).get().getUser().getId() != null);

        // assert the post
        assert(this.savedPostRepository.findById(savedPost.getId()).get().getPost().getId() != null);

    }
}
