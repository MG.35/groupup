package it.unimib.groupup.unit;

import it.unimib.groupup.model.MyGroup;
import it.unimib.groupup.model.Post;
import it.unimib.groupup.model.User;
import it.unimib.groupup.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class MyGroupUTest {

    @Autowired
    CommentLeafRepository commentLeafRepository;
    @Autowired
    CommentRootRepository commentRootRepository;
    @Autowired
    MyGroupRepository myGroupRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void testMyGroupWithoutOtherEntities() {
        MyGroup group = new MyGroup();
        group.setId(88);
        group.setName("group");
        group.setDescription("description");
        group.setPhoto("photo");

        // check if the group is instantiated correctly
        String toStringExpected = "MyGroup{id=88, name='group', description='description', photo='photo'}";
        assert (group.toString().equals(toStringExpected));

        // save the group
        MyGroup savedGroup = myGroupRepository.save(group);

        // check if the group is saved correctly
        Optional<MyGroup> foundGroup = myGroupRepository.findById(savedGroup.getId());
        assert (foundGroup.isPresent());
        String toStringExpected2 = "MyGroup{id=" + foundGroup.get().getId() + ", name='group', description='description', photo='photo'}";
        assert (foundGroup.get().toString().equals(toStringExpected2));

        // delete the group
        myGroupRepository.delete(group);

        // check if the group is deleted correctly
        Optional<MyGroup> foundGroup2 = myGroupRepository.findById(group.getId());
        assert (foundGroup2.isEmpty());

    }

    @Test
    @Transactional
    void testMyGroupWithOtherEntities() {
        MyGroup group = new MyGroup();
        group.setName("group");
        group.setDescription("description");

        // save the group
        myGroupRepository.save(group);

        // instantiate a post
        Post post = new Post();
        post.setMessage("message");
        post.setMyGroup(group);

        // instantiate a user
        User user = new User();
        user.setUsername("username");
        Set<MyGroup> groups = new java.util.HashSet<>();
        groups.add(group);
        user.setMyGroups(groups);

        // save the post
        postRepository.save(post);

        // save the user
        userRepository.save(user);

        // save the group
        myGroupRepository.save(group);

        // check if user has the group
        Optional<User> foundUser = userRepository.findById(user.getId());
        assert (foundUser.isPresent());
        assert (foundUser.get().getMyGroups().contains(group));

        // check if post has the group
        Optional<Post> foundPost = postRepository.findById(post.getId());
        assert (foundPost.isPresent());
        assert (foundPost.get().getMyGroup().equals(group));

    }

}

