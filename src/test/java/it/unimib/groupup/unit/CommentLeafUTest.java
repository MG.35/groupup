package it.unimib.groupup.unit;

import it.unimib.groupup.model.CommentLeaf;
import it.unimib.groupup.model.CommentRoot;
import it.unimib.groupup.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class CommentLeafUTest {

    @Autowired
    CommentLeafRepository commentLeafRepository;
    @Autowired
    CommentRootRepository commentRootRepository;
    @Autowired
    MyGroupRepository myGroupRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void testCommentLeadWithoutOtherEntities() {
        CommentLeaf commentLeaf = new CommentLeaf();
        commentLeaf.setId(177);
        commentLeaf.setSentence("test");

        CommentLeaf commentLeafSaved = commentLeafRepository.save(commentLeaf);

        String toStringExpected = "Comment{id=177, sentence='test'}";
        String toStringActual = commentLeaf.toString();
        assert toStringExpected.equals(toStringActual);

        Optional<CommentLeaf> commentLeafFound = commentLeafRepository.findById(commentLeafSaved.getId());
        assert commentLeafFound.isPresent();
        String toStringExpected2 = "Comment{id="+commentLeafFound.get().getId()+", sentence='test'}";
        String toStringActual2 = commentLeafFound.get().toString();
        assert toStringExpected2.equals(toStringActual2);

        commentLeafRepository.delete(commentLeaf);
        Optional<CommentLeaf> commentLeafFound2 = commentLeafRepository.findById(commentLeaf.getId());
        assert commentLeafFound2.isEmpty();
    }

    @Test
    @Transactional
    void testCommentLeadWithOtherEntities() {
        CommentLeaf commentLeaf = new CommentLeaf();

        CommentRoot commentRoot = new CommentRoot();
        Set<CommentLeaf> commentLeafs = new java.util.HashSet<>();
        commentLeafs.add(commentLeaf);
        commentRoot.setComments(commentLeafs);

        // save commentLeaf
        commentLeafRepository.save(commentLeaf);

        // save commentRoot
        commentRootRepository.save(commentRoot);

        // check commentRoot has commentLeafs
        Optional<CommentRoot> commentRootFound = commentRootRepository.findById(commentRoot.getId());
        assert commentRootFound.isPresent();
        assert commentRootFound.get().getComments().contains(commentLeaf);



    }
}
