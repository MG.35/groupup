package it.unimib.groupup.unit;

import it.unimib.groupup.model.MyLabel;
import it.unimib.groupup.model.User;
import it.unimib.groupup.repositories.MyLabelsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class MyLabelUTest {

    @Autowired
    MyLabelsRepository myLabelsRepository;

    @Test
    void testPostWithoutOtherEntities() {
        MyLabel myLabels = new MyLabel();

        myLabels.setLabel("test");

        // save the myLabels
        myLabelsRepository.save(myLabels);

        // check if the myLabels is saved
        assert(myLabelsRepository.findById(myLabels.getId()).isPresent());
    }

    @Test
    @Transactional
    void testPostWithOtherEntities() {
        MyLabel myLabels = new MyLabel();

        myLabels.setLabel("test");

        // create a new user
        User user = new User();
        // save the user
        user.setUsername("test");

        myLabels.setUser(user);

        // save the myLabels
        myLabelsRepository.save(myLabels);

        // check if the myLabels is saved
        assert(myLabelsRepository.findById(myLabels.getId()).isPresent());

        // check if the user is present in myLabels
        assert(myLabels.getUser().getUsername().equals("test"));
    }
}
