package it.unimib.groupup.unit;

import it.unimib.groupup.model.*;
import it.unimib.groupup.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
// change the properties of the application to test properties

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
class UserUTest {

    @Autowired
    CommentLeafRepository commentLeafRepository;
    @Autowired
    CommentRootRepository commentRootRepository;
    @Autowired
    MyGroupRepository myGroupRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void testUserWithoutOtherEntities() {
        // instantiate a user
        User user = new User();
        user.setId(55);
        user.setUsername("test");
        user.setPassword("test");
        user.setEmail("mail");
        user.setName("name");
        user.setSurname("surname");
        user.setPhoto("photo");

        // check if the user is instantiated correctly
        String toStringExpected = "" +
                "User{id=55," +
                " name='name'," +
                " surname='surname'," +
                " email='mail'," +
                " username='test'}";
        assert (Objects.equals(user.toString(), toStringExpected));

        // save the user
        User savedUser = userRepository.save(user);

        // check if the user is saved correctly
        Optional<User> foundUser = userRepository.findById(savedUser.getId());
        assert (foundUser.isPresent());
        String toStringExpected2 = "" +
                "User{id=" + savedUser.getId() + "," +
                " name='name'," +
                " surname='surname'," +
                " email='mail'," +
                " username='test'}";
        assert (Objects.equals(foundUser.get().toString(), toStringExpected2));

        // delete the user
        userRepository.delete(savedUser);

        // check if the user is deleted correctly
        Optional<User> userDeleted = userRepository.findById(savedUser.getId());
        assert (userDeleted.isEmpty());

    }

    @Test
    @Transactional
    void testUserWithOtherEntities() {
        // instantiate a user
        User user = new User();
        user.setUsername("test");
        user.setPassword("test");
        user.setEmail("mail");
        user.setName("name");
        user.setSurname("surname");
        user.setPhoto("photo");

        // save the user
         User savedUser = userRepository.save(user);

        // instantiate a post
        Post post = new Post();
        post.setMessage("test");
        post.setAuthor(savedUser);

        // instantiate a comment
        CommentRoot commentRoot = new CommentRoot();
        commentRoot.setSentence("test");
        commentRoot.setAuthor(savedUser);
        commentRoot.setPost(post);

        // instantiate a comment leaf
        CommentLeaf commentLeaf = new CommentLeaf();
        commentLeaf.setSentence("test");
        commentLeaf.setAuthor(savedUser);
        commentLeaf.setCommentRoot(commentRoot);

        // instantiate my group
        MyGroup myGroup = new MyGroup();
        myGroup.setName("test");
        // new set of users
        Set<User> users = new java.util.HashSet<User>();
        users.add(savedUser);
        myGroup.setUsers(users);


        // save the post
        postRepository.save(post);

        // save the comment
        commentRootRepository.save(commentRoot);

        // save the comment leaf
        commentLeafRepository.save(commentLeaf);

        // save the group
        myGroupRepository.save(myGroup);

        // save the user
        userRepository.save(user);

        // check if the user is saved correctly
        Optional<User> userSaved = userRepository.findById(user.getId());
        assert (userSaved.isPresent());

        // check that post has the user
        Optional<Post> postSaved = postRepository.findById(post.getId());
        assert (postSaved.isPresent());
        assert (Objects.equals(postSaved.get().getAuthor().getId(), savedUser.getId()));

        // check that comment has the user
        Optional<CommentRoot> commentRootSaved = commentRootRepository.findById(commentRoot.getId());
        assert (commentRootSaved.isPresent());
        assert (Objects.equals(commentRootSaved.get().getAuthor().getId(), savedUser.getId()));

        // check that comment leaf has the user
        Optional<CommentLeaf> commentLeafSaved = commentLeafRepository.findById(commentLeaf.getId());
        assert (commentLeafSaved.isPresent());
        assert (Objects.equals(commentLeafSaved.get().getAuthor().getId(), savedUser.getId()));

        // check that group has the user
        Optional<MyGroup> myGroupSaved = myGroupRepository.findById(myGroup.getId());
        assert (myGroupSaved.isPresent());
        assert (Objects.equals(myGroupSaved.get().getUsers().iterator().next().getId(), savedUser.getId()));

    }

}

