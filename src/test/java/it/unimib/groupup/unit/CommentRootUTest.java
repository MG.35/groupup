package it.unimib.groupup.unit;

import it.unimib.groupup.model.CommentLeaf;
import it.unimib.groupup.model.CommentRoot;
import it.unimib.groupup.model.Post;
import it.unimib.groupup.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class CommentRootUTest {

    @Autowired
    CommentLeafRepository commentLeafRepository;
    @Autowired
    CommentRootRepository commentRootRepository;
    @Autowired
    MyGroupRepository myGroupRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void testCommentRootWithoutOtherEntities() {
        CommentRoot commentRoot = new CommentRoot();
        commentRoot.setId(77);
        commentRoot.setSentence("comment");

        String toStringExpected = "Comment{id=77, sentence='comment'}";
        String toStringActual = commentRoot.toString();
        assert(toStringActual.equals(toStringExpected));

        // save commentRoot
        CommentRoot commentRootSaved =commentRootRepository.save(commentRoot);

        // check if commentRoot is saved
        Optional<CommentRoot> foundCommentRoot = commentRootRepository.findById(commentRootSaved.getId());
        assert(foundCommentRoot.isPresent());
        String toStringExpected2 = "Comment{id="+foundCommentRoot.get().getId()+", sentence='comment'}";
        String toStringActual2 = foundCommentRoot.get().toString();
        assert(toStringActual2.equals(toStringExpected2));

        // delete commentRoot
        commentRootRepository.delete(commentRootSaved);
        Optional<CommentRoot> foundCommentRoot2 = commentRootRepository.findById(commentRootSaved.getId());
        assert(foundCommentRoot2.isEmpty());
    }

    @Test
    @Transactional
    void testCommentRootWithOtherEntities() {
        CommentRoot commentRoot = new CommentRoot();
        commentRoot.setSentence("test");

        commentRootRepository.save(commentRoot);

        // instantiate comment leaf
        CommentLeaf commentLeaf = new CommentLeaf();
        commentLeaf.setCommentRoot(commentRoot);

        // instantiate post
        Post post = new Post();
        Set<CommentRoot> commentRoots = new java.util.HashSet<>();
        commentRoots.add(commentRoot);
        post.setComments(commentRoots);

        // save comment leaf
        commentLeafRepository.save(commentLeaf);

        // save post
        postRepository.save(post);

        // save comment root
        commentRootRepository.save(commentRoot);

        // check if post has comment root
        Optional<Post> foundPost = postRepository.findById(post.getId());
        assert(foundPost.isPresent());
        assert(foundPost.get().getComments().contains(commentRoot));

        // check if comment leaf has comment root
        Optional<CommentLeaf> foundCommentLeaf = commentLeafRepository.findById(commentLeaf.getId());
        assert(foundCommentLeaf.isPresent());
        assert(foundCommentLeaf.get().getCommentRoot().equals(commentRoot));



    }
}
