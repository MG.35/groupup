import { createContext } from 'react';

/**
 * UserContext
 * @type {React.Context<unknown>}
 */
const UserContext = createContext();
export default UserContext;