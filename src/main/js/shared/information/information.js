import React from "react";
import AddFriendButton from "../../profile/add_friend_button";
import JoinGroupButton from "../../group/join_group_button";

/**
 * Style for rounded image
 * @type {{WebkitBorderRadius: string, borderRadius: string, MozBorderRadius: string, display: string, width: string, backgroundSize: string, height: string}}
 */
const round_image = {
    width: "250px",
    height: "250px",
    backgroundSize: "cover",
    display: "block",
    borderRadius: "125px",
    WebkitBorderRadius: "125px",
    MozBorderRadius: "125px",
}

/**
 * This component is used to display the information of a user.
 * @param {Object} props - Information about the user.
 * @returns {JSX.Element}
 */
export function Information(props) {

    /**
     * Returns the information of the user.
     */
    return (
        <div className="container d-flex p-3" id="information">


            <img style={round_image} src={props.photo} alt="personal-image" id="photo"/>


            <div className="h1 d-flex flex-column justify-content-center ms-5 w-100" id="name">

                <div className="d-flex justify-content-between">

                    <span>
                        {props.name}
                    </span>

                    {
                        props.currentuserid ? (
                            <AddFriendButton userid={props.userid} currentuserid={props.currentuserid}/>
                        ) : (<></>)
                    }

                    {
                        props.groupid ? (
                            <JoinGroupButton groupid={props.groupid} userid={props.userid}/>
                        ) : (<></>)
                    }
                </div>
                {
                    props.username ? (

                            <small className="text-muted" id="username">
                                {"@" + props.username}
                            </small>
                        )
                        :
                        (<></>)
                }

            </div>


        </div>


    )
}