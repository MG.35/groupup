import React from "react";
import {Card, CardBody, Col, NavLink, Row} from "reactstrap";

/**
 * Comment component
 * @param props
 * @returns {JSX.Element}
 */
function Comment(props) {

    /**
     * This state is used to save the author of the comment
     */
    const [author, setAuthor] = React.useState(null)

    /**
     * This function is used to get the author of the comment
     */
    function getAuthor() {
        fetch(props.comment._links.author.href)
            .then(response => response.json())
            .then(data => {
                setAuthor(data)
            })
    }

    /**
     * useEffect hook
     */
    React.useEffect(
        () => {
            getAuthor()
        }, [!author]
    )

    /**
     * Render the component
     */
    return (
        <div>
            {author && <Card className={"border-0"}>
                <CardBody className={"pb-0"}>
                    <Row>
                        <Col sm="1">
                            <NavLink className="d-inline-block" style={{padding: "0px 0px 0px 0px", width: "auto"}} href={author._links && `/profile/${author._links.self.href.split('/').pop()}`}>
                                <img src={author.photo} style={{height: "30px", borderRadius: "20px"}} alt="author-photo"/>
                            </NavLink>
                        </Col>
                        <Col sm="11">
                            <p>
                                <NavLink className="d-inline-block" style={{padding: "0px 0px 0px 0px", width: "auto"}} href={author._links && `/profile/${author._links.self.href.split('/').pop()}`}>
                                    <small style={{fontWeight: "bold"}}>{author.name}&nbsp;{author.surname}</small>
                                </NavLink>
                                &nbsp;
                                <small className="text-muted"
                                       id={"commentRoot-" + props.comment._links.self.href.split('/').pop()}
                                       data-testid={"commentRoot-" + props.comment._links.self.href.split('/').pop()}>{props.comment.sentence}</small>
                            </p>
                        </Col>
                    </Row>
                </CardBody>
            </Card>}
        </div>
    );

}

export default Comment;