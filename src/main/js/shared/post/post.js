import React, {useContext} from "react";
import {
    Button,
    Card,
    CardBody,
    CardSubtitle,
    CardText,
    CardTitle,
    Collapse,
    Input,
    InputGroup,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, NavLink
} from 'reactstrap';
import Moment from "moment";
import UserContext from "../user-context/user_context";
import Select from "react-select";
import Comment from "./comment";

/**
 * Post component
 * @param props
 * @returns {JSX.Element}
 */
function Post(props) {

    /**
     * @user represents the current logged user.
     */
    let loggedUser = useContext(UserContext);

    /**
     * This state is used to store the author of the post
     */
    const [author, setAuthor] = React.useState([]);

    /**
     * This state is used to store the group of the post
     */
    const [group, setGroup] = React.useState(null);

    /**
     * This is the id of the post
     */
    let postId = props.post._links.self.href.split("/").pop();

    /**
     * This is a boolean to know if the post has been posted on a group or on a user's page
     */
    let postedOnGroup = true;

    /**
     * This state is used to store the post's saved post
     */
    const [savedPost, setSavedPost] = React.useState(null);

    /**
     * This state is used to store if the post has a saved post
     */
    const [hasSavedPost, setHasSavedPost] = React.useState(null);

    /**
     * This state is used to store the post's label
     */
    const [label, setLabel] = React.useState(null);

    /**
     * This state is used to store the selected post's label
     */
    const [selectedLabel, setSelectedLabel] = React.useState(null);

    /**
     * This state is used to store the label's select input
     */
    const [inputLabel, setInputLabel] = React.useState(null);

    /**
     * This state is used to control the label's modal
     */
    const [showLabel, setShowLabel] = React.useState(false);

    /**
     * This state is used to store the users that put a like to the post
     */
    const [likeUsers, setLikeUsers] = React.useState([]);

    /**
     * This state is used to store the number of likes for the post
     */
    const [nLikes, setNLikes] = React.useState(0);

    /**
     * This state is used to store if the logged user likes the post
     */
    const [liked, setLiked] = React.useState(false);

    /**
     * This state is used to store the input to the new comment
     */
    const [inputComment, setInputComment] = React.useState("");

    /**
     * This state is used to control the comments visibility
     */
    const [showComments, setShowComments] = React.useState(false);

    /**
     * This state is used to store posts comments
     */
    const [comments, setComments] = React.useState([]);

    /**
     * HTTP request to get the author or group of the post
     * @param url
     * @returns {Promise<any>}
     */
    function get_data(url) {
        return fetch(url)
            .then(response => response.json())
            .then(data => {
                return data
            });
    }

    /**
     * HTTP request to get the comments of the post
     */
    function updateComments() {
        get_data(props.post._links.comments.href)
            .then(data => {
                setComments(data._embedded.commentRoots)
            })
    }

    /**
     * HTTP request to get the label of the post
     */
    function get_label() {
        return fetch(`/api/labels/search/post-label?userId=${loggedUser.id}&postId=${props.post._links.self.href.split('/').pop()}`)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * HTTP request to get the label of the post
     */
    function get_saved_post() {
        return fetch(`/api/savedPosts/search/saved-post?userId=${loggedUser.id}&postId=${props.post._links.self.href.split('/').pop()}`)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * This function is used to toggle the visibility of the comments
     */
    function toggleShowComment() {
        setShowComments(() => !showComments);
    }

    /**
     * This function is used to save the comment to the post
     */
    function insertComment() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                sentence: inputComment,
                author: `/api/users/${loggedUser.id}`,
                post: props.post._links.self.href
            })
        };

        /**
         * If the comment is empty, do nothing
         */
        if (inputComment === "") return;

        /**
         * HTTP request to get the main comment of the post
         */
        fetch('/api/commentRoots', requestOptions)
            .then(() => {
                setInputComment("")
                if (showComments) updateComments()
            })
            .catch(error => console.error('There was an error!', error));
    }


    /**
     * This function is used to get the likes information for the post and update the state
     */
    function updateLikeState() {
        get_data(props.post._links.likedBy.href)
            .then(data => {
                    let userIds = []
                    let contains = false
                    data._embedded.users.forEach(u => {
                        userIds.push(u._links.self.href)
                        if (parseInt(u._links.self.href.split("/").pop()) === loggedUser.id) contains = true;
                    })
                    setLikeUsers(userIds)
                    setLiked(contains)
                    setNLikes(userIds.length)
                }
            );
    }

    /**
     * This function is used to remove the logged user like to the post
     */
    function removeLike() {
        const requestOptions = {method: 'DELETE'};

        fetch(`${props.post._links.likedBy.href}/${loggedUser.id}`, requestOptions)
            .then(() => {
                updateLikeState()
            })
            .catch(error => console.error('There was an error!', error));
    }

    /**
     * This function is used to put the logged user like to the post
     */
    function putLike() {
        let usersFormat = `/api/users/${loggedUser.id}`
        likeUsers.forEach(user => usersFormat += `\n${user}`)
        const requestOptions = {
            method: 'PUT',
            headers: {'Content-Type': 'text/uri-list'},
            body: usersFormat
        };

        fetch(`${props.post._links.likedBy.href}`, requestOptions)
            .then(() => {
                updateLikeState()
            })
            .catch(error => console.error('There was an error!', error));
    }

    /**
     * This function is used fot the like toggle of the post
     */
    function toggleLike() {
        if (liked) removeLike()
        else putLike()
    }

    /**
     * This function is used to get the comments of the post
     */
    React.useEffect(
        () => {
            if (showComments) updateComments()
        }, [!showComments]
    )

    /**
     * This function is used to get the author of the post,
     * and is called when the component is mounted or author state is updated
     */
    React.useEffect(
        () => {
            get_data(props.post._links.author.href)
                .then(data => {
                        return setAuthor(data);
                    }
                );
        }, [!author]
    )

    /**
     * This function is used to get the saved-post of the post seed by a user,
     * and is called when the component is mounted or author state is updated
     */
    React.useEffect(
        () => {
            get_saved_post()
                .then(data => {
                        setHasSavedPost(!!data)
                        return setSavedPost(data);
                    }
                );
        }, [!savedPost && !hasSavedPost]
    )

    /**
     * This function is used to get the group of the post,
     * and is called when the component is mounted or author state is updated
     */
    React.useEffect(
        () => {
            get_data(props.post._links.myGroup.href)
                .then(data => {
                    postedOnGroup = true;
                    return setGroup(data);
                })
                .catch(() => {
                    postedOnGroup = false;
                    return null;
                });
        }, [!group && postedOnGroup]
    )

    /**
     * This function is used to update the like state,
     * and is called when the component is mounted or the like state is updated
     */
    React.useEffect(
        () => {
            updateLikeState()
        }, [!likeUsers, !liked, !nLikes]
    )

    /**
     * This function is used to get the label of the post,
     * and is called when the component is mounted or author state is updated
     */
    React.useEffect(
        () => {
            if (props.label) {
                let label = loggedUser.myLabels.find(label => {
                    return label.label === props.label
                })
                setSelectedLabel(label)
                setLabel(label)
            } else {
                get_label()
                    .then(label => {
                        setLabel(label)
                        if (!selectedLabel) {
                            setSelectedLabel(label)
                        }
                    })
            }
        }, [!label]
    )


    /**
     * Function used to toggle the label's modal
     */
    function toggleLabelModal() {
        setShowLabel(!showLabel)
        setSelectedLabel(label)
    }

    /**
     * Function used after the label is saved
     */
    function saveLabel() {
        if (props.label) {
            if (selectedLabel) {
                updateSavedPost()
            }
            props.removeLabel()
        } else {
            if (isBlank(selectedLabel)) {
                if (savedPost) {
                    removeSavedPost()
                    setSavedPost(null)
                    setLabel(null)
                }
            } else {
                updateSavedPost()
            }
        }
        toggleLabelModal()
    }

    /**
     * This function is used to remove the post from the label
     */
    const removeSavedPost = () => {
        fetch(`${savedPost._links.self.href}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(
            () => {
                setSavedPost(null);
                setLabel(null);
            }
        )
    }

    /**
     * This function is used to remove the post from the label
     */
    const updateSavedPost = () => {
        let newLabel = loggedUser.myLabels.find(label => {
            return label.label === selectedLabel.label
        })

        if (savedPost) {
            removeSavedPost()
        }

        let newSavedPost = {
            label: `/api/labels/${newLabel._links.self.href.split('/').pop()}`,
            user: `/api/users/${loggedUser.id}`,
            post: `/api/posts/${props.post._links.self.href.split('/').pop()}`
        }
        fetch(`/api/savedPosts`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newSavedPost)
        }).then(
            (response) => response.json()
        ).then(
            (data) => {
                setSavedPost(data);
                setLabel(newLabel);
            }
        )
    }

    /**
     * Check if a label is blank
     */
    function isBlank(label) {
        return !label || /^\s*$/.test(label)
    }

    /**
     * Function used to check if label's select input has been changed
     * @param input - new input of the select
     */
    function handleSelectInputChange(input) {
        setInputLabel(input)
    }

    /**
     * Function used to add a new label on click
     */
    function addLabel() {

        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                label: inputLabel,
                user: `/api/users/${loggedUser.id}`
            })
        };

        fetch(`/api/labels`, requestOptions)
            .then(response => response.json())
            .then(newLabel => {
                loggedUser.myLabels.push(newLabel)
                setInputLabel({...inputLabel})
                return newLabel
            })

    }

    /**
     * Function used to add a new label is selected
     */
    function changeLabel(newLabel) {
        setSelectedLabel(newLabel)
    }

    /**
     * Function to compare two labels
     * @param label1 - first label to campare
     * @param label2 - second label to campare
     */
    function labelCompare(label1, label2) {
        return label1.value.localeCompare(label2.value)
    }

    /**
     * Shadow style
     * @type {{boxShadow: string, WebkitBoxShadow: string, MozBoxShadow: string}}
     */
    const shadow_style = {
        WebkitBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        MozBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        boxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        width: "70%"
    }

    /**
     * This function is used to handle the remove label button
     * @returns {JSX.Element}
     * @constructor
     */
    const CustomClearText = () => <Button
        id={"remove-post-label"}
        data-testid={"remove-label"}
        onClick={() => setSelectedLabel(null)}
    >
        <span className={"fa fa-ban"}/>
    </Button>;

    /**
     * This function is used to handle the add label button
     * @returns {JSX.Element}
     * @constructor
     */
    const ClearIndicator = (props) => {
        const {
            children = <CustomClearText/>,
            getStyles,
            innerProps: {ref, ...restInnerProps},
        } = props;
        return (
            <div
                {...restInnerProps}
                ref={ref}
                style={getStyles('clearIndicator', props)}
            >
                <div style={{padding: '0px 5px'}}>{children}</div>
            </div>
        );
    };


    /**
     * Render the post
     */
    return (
        <div>
            <Card id={"post-" + postId} className="m-3 mx-auto" body style={shadow_style}
                  outline>
                <CardBody>
                    <CardTitle className="d-flex justify-content-between">

                        <NavLink className="d-flex align-items-center" style={{paddingLeft: "0px", width: "auto"}}
                                 href={author._links && `/profile/${author._links.self.href.split('/').pop()}`}>
                            <img src={author.photo} alt="personal-image"
                                 style={{height: "50px", borderRadius: "20px"}}/>
                            <div className="d-inline-block ms-3">
                                <h5 className="mb-0"
                                    id={"authorName-" + postId}>{author.name + " " + author.surname}</h5>
                                <small className="text-muted"
                                       id={"authorUsername-" + postId}>{"@" + author.username}
                                </small>
                            </div>
                        </NavLink>

                        <div>
                            {author._links && parseInt(author._links.self.href.split('/').pop()) === loggedUser.id &&
                                <Button style={{background: "white", borderColor: "white", color: "#a7b3bf"}}
                                        className="float-end"
                                        id={"remove-post-" + postId}
                                        size="lg"
                                        data-testid={"remove-" + postId}
                                        onClick={() => props.removePost(props.post._links.self.href)}
                                >
                                    <span className={"fa fa-trash"}/>
                                </Button>
                            }
                            <Button
                                onClick={toggleLabelModal}
                                style={{
                                    background: "white",
                                    borderColor: "white",
                                    color: `${label ? "#03CCACFF" : "#a7b3bf"}`
                                }}
                                outline={!label}
                                size="lg"
                                id={"labelButton-" + postId}>
                                <span className={"fa fa-star"}/>
                            </Button>
                        </div>

                    </CardTitle>
                    <CardSubtitle className="mb-2 text-muted" id={"postDate-" + postId}
                                  data-testid={"postDate-" + postId}>
                        <small className="text-muted"> has written on {group ? group.name : "their page"}</small>
                    </CardSubtitle>


                    <CardText className="fs-6" id={"postMessage-" + postId} data-testid={"postMessage-" + postId}>
                        {props.post.message}
                    </CardText>

                    <div className="mt-5 d-flex justify-content-between align-items-end">
                        <small className="text-muted">
                            {Moment(props.post.date).format('DD-MM-yy hh:mm')}
                        </small>
                        <Button style={{
                            background: "white",
                            borderColor: "#03CCAC",
                            color: `${liked ? "#03CCACFF" : "#a7b3bf"}`
                        }}

                                id={"like-" + postId}
                                data-testid={"like-" + postId}

                                onClick={toggleLike}>
                            {nLikes}

                            <span className={`fa ${liked ? "fa-thumbs-up" : "fa-thumbs-o-up"} ms-2`}/>
                        </Button>
                    </div>

                    <div style={{paddingTop: "5px", clear: "both"}}>
                        <hr className="mb-4"/>
                    </div>
                    <InputGroup>
                        <img src={loggedUser.photo} alt="personal-image"
                             style={{height: "40px", borderRadius: "20px"}}/>
                        <Input id={"comment-" + postId}
                               data-testid={"comment-" + postId}
                               placeholder="Write a comment..."
                               value={inputComment}
                               onChange={e => {
                                   setInputComment(e.target.value)
                               }}
                               onKeyPress={e => {
                                   if (e.key === 'Enter') {
                                       insertComment()
                                   }
                               }}
                        />
                        <Button style={{background: "#a7b3bf", border: "#03CCAC", color: "white"}}
                                onClick={insertComment}><span className={"fa fa-paper-plane"}/></Button>
                    </InputGroup>

                    <div style={{marginTop: "15px"}}
                         id={"toggleComments-" + postId}
                         data-testid={"toggleComments-" + postId}>
                        <a style={{color: "grey", textDecoration: "underline", cursor: "pointer"}}
                           onClick={toggleShowComment}>
                            {showComments ? "hide comments..." : "show comments..."}
                        </a>
                    </div>
                    <Collapse isOpen={showComments}
                              id={"collapseComments-" + postId}
                              data-testid={"collapseComments-" + postId}>
                        {comments.map((comment, index) => <Comment key={index} comment={comment}/>)}
                    </Collapse>

                </CardBody>
            </Card>
            <Modal isOpen={showLabel} toggle={toggleLabelModal}>
                <ModalHeader toggle={toggleLabelModal}>Post's label</ModalHeader>
                <ModalBody>
                    <div id={"label-modal"}>
                        <Select
                            onInputChange={handleSelectInputChange}
                            defaultValue={selectedLabel}
                            components={{ClearIndicator}}
                            isClearable={true}
                            value={selectedLabel}
                            onChange={changeLabel}
                            options={loggedUser.myLabels && !loggedUser.myLabels.find(label => {
                                return label.label === inputLabel
                            }) && !isBlank(inputLabel) ? [...loggedUser.myLabels.map(
                                labelOption => {
                                    return {
                                        value: labelOption.label,
                                        label: labelOption.label
                                    }
                                }), {
                                value: String(inputLabel),
                                label: <span>{inputLabel} <Button color="primary" onClick={addLabel}>+</Button></span>,
                                isDisabled: true
                            }].sort(labelCompare) : loggedUser.myLabels && loggedUser.myLabels.map(
                                labelOption => {
                                    return {
                                        value: labelOption.label,
                                        label: labelOption.label
                                    }
                                }).sort(labelCompare)}
                        />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" id="save-label" onClick={saveLabel}>Save</Button>{' '}
                    <Button color="secondary" id="cancel-save-label" onClick={toggleLabelModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default Post;