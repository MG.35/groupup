import React from "react";
import Post from "./post";

/**
 * This component is responsible for rendering the list of posts.
 * @param props
 * @returns {JSX.Element}
 */
function Posts(props) {

    /**
     * This function is responsible for deleting a post.
     * @param url The url of the post to be deleted.
     */
    function removePost(url) {
        const requestOptions = {method: 'DELETE'};

        fetch(url, requestOptions)
            .then(() => {
                window.location.reload();
            })
            .catch(error => console.error('There was an error in deleting the post', error));
    }

    /**
     * This function is responsible for rendering the list of posts.
     * @param posts
     * @returns {JSX.Element}
     */
    function post_list(posts) {
        return posts.map((post, index) => {
            return (
                <Post
                    key={index}
                    post={post}
                    removePost={removePost}
                />
            )
        });
    }

    /**
     * Render the list of posts.
     */
    return (
        <div className="d-flex justify-content-around">
            <div className="col-md-8" id="posts">
                {post_list(props.posts)}
            </div>
        </div>
    );
}

export default Posts;
