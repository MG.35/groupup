import ReactDOM from 'react-dom';
import React from 'react';
import Section from './navigation/routing/sections';


/**
 * Main app component, contains all other components
 */
ReactDOM.render(
    <Section/>,
    document.getElementById('root')
);