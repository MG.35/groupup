import React, {useContext} from "react";
import {Nav, NavbarBrand, NavItem, NavLink} from "reactstrap";
import UserContext from "../../shared/user-context/user_context";

/**
 * This component is the navigation bar for the application.
 * @returns {JSX.Element}
 */
export default function Navbar() {


    /**
     * This function is used to get the user context.
     */
    let user = useContext(UserContext);

    /**
     * Render the navigation bar.
     */
    return (
        <div>
            <Nav className="pb-1">
                <NavItem className="ms-3 mt-2 me-auto" id="navbar-brand">
                    <NavbarBrand href="/board">
                        <img src="/logo.png" alt="logo" width="50" height="50"/>
                    </NavbarBrand>
                </NavItem>

                {/*<NavItem className="mt-3" id="navbar-search">*/}
                {/*    <InputGroup>*/}
                {/*        <InputGroupText>*/}
                {/*            <img src="/search-icon.svg" alt="search" width="17" height="17"/>*/}
                {/*        </InputGroupText>*/}
                {/*        <Input placeholder="search on GroupUp"/>*/}
                {/*    </InputGroup>*/}
                {/*</NavItem>*/}

                <NavItem className="mt-2 ms-auto" id="navbar-board">
                    <NavLink href="/board">
                        <img src="/home-icon.svg" alt="board" width="30" height="30"/>
                    </NavLink>
                </NavItem>

                <NavItem className="mt-2" id="navbar-group">
                    <NavLink href="/group/list">
                        <img src="/group-icon.svg" alt="group" width="30" height="30"/>
                    </NavLink>
                </NavItem>


                <NavItem className="mt-2 me-auto" id="navbar-saved-posts">
                    <NavLink href={`/saved-posts`}>
                        <img src="/saved-posts-icon.svg" alt="saved-posts" width="30" height="30"/>
                    </NavLink>
                </NavItem>

                <NavItem className="mt-2 ms-auto" id="navbar-profile">
                    <NavLink href={`/profile/${user.id}`}>
                        <img src="/user-icon.svg" alt="profile" width="30" height="30"/>
                    </NavLink>
                </NavItem>

                <NavItem className="me-3 mt-2" id="navbar-logout">
                    <NavLink href={`/logout`}>
                        <img src="/logout-icon.svg" alt="logout" width="30" height="30"/>
                    </NavLink>
                </NavItem>

            </Nav>
        </div>
    );
}