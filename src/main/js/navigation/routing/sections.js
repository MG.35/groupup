import {BrowserRouter, Routes, Route} from "react-router-dom";
import React from 'react';
import Home from "../../home/home";
import Group from "../../group/group";
import Profile from "../../profile/profile";
import GroupList from "../../group/group_list";
import GroupCreate from "../../group/group_create";
import Board from "../../board/board";
import SavedLabels from "../../saved_posts_with_labels/saved_labels";
import SavedPosts from "../../saved_posts_with_labels/saved_posts";
import UserContext from "../../shared/user-context/user_context";
import LabelCreate from "../../labels/label_create";
import Loading from "./loading";

/**
 * This component is responsible for routing
 * @returns {JSX.Element}
 */
function Sections() {

    /**
     * This variable is responsible to temporarily store the logged user data
     * @type {*[]}
     */
    let user = []

    /**
     * This variable is responsible to permanently store the logged user data
     */
    const [userState, setUserState] = React.useState([]);

    /**
     * This function is responsible to get the logged user data without the labels
     * @returns {Promise<void>}
     */
    const get_user = () => fetch('/whoami')
        .then(response => response.json())
        .then(data => {
            user = data
            return data
        })

    /**
     * This function is responsible to get the logged user labels
     * @returns {Promise<void>}
     */
    const get_labels = () => fetch(`/api/users/${user.id}/myLabels`)
        .then(response => response.json())
        .then(data => {
                user.myLabels = data._embedded.myLabels;
                return user
            }
        );

    /**
     * use effect is responsible to call the get_user function and the get_labels function
     * then set the userState.
     */
    React.useEffect(() => {
        if (window.location.pathname !== '/') {
            get_user()
                .then(data => setUserState(data))
                .then(() => get_labels())
                .then(data => setUserState(data))
        }
    }, []);


    /**
     * wait for the userState to be set and then render the sections opportunely
     */
    if (userState.length === 0) {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/board" element={<Loading/>}/>
                    <Route path="/profile/:id" element={<Loading/>}/>
                    <Route path="/group/:id" element={<Loading/>}/>
                    <Route path="/group/list" element={<Loading/>}/>
                    <Route path="/group/create" element={<Loading/>}/>
                    <Route path="/saved-posts" element={<Loading/>}/>
                    <Route path="/saved-posts/:label" element={<Loading/>}/>
                    <Route path="/label/create" element={<Loading/>}/>
                </Routes>
            </BrowserRouter>
        )
    } else {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/board"
                           element={<UserContext.Provider value={userState}><Board/></UserContext.Provider>}/>
                    <Route path="/profile/:id"
                           element={<UserContext.Provider value={userState}><Profile/></UserContext.Provider>}/>
                    <Route path="/group/:id"
                           element={<UserContext.Provider value={userState}><Group/></UserContext.Provider>}/>
                    <Route path="/group/list"
                           element={<UserContext.Provider value={userState}><GroupList/></UserContext.Provider>}/>
                    <Route path="/group/create"
                           element={<UserContext.Provider value={userState}><GroupCreate/></UserContext.Provider>}/>
                    <Route path="/saved-posts"
                           element={<UserContext.Provider value={userState}><SavedLabels/></UserContext.Provider>}/>
                    <Route path="/saved-posts/:label"
                           element={<UserContext.Provider value={userState}><SavedPosts/></UserContext.Provider>}/>
                    <Route path="/label/create"
                           element={<UserContext.Provider value={userState}><LabelCreate/></UserContext.Provider>}/>
                </Routes>
            </BrowserRouter>
        )
    }


}


export default Sections;
