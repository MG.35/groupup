import React from 'react';

/**
 * This component is responsible for rendering the loading screen.
 * @returns {JSX.Element}
 */
function Loading() {

    /**
     * Render the loading screen.
     */
    return (
        <h1>Loading</h1>
    )
}

export default Loading;