import React from "react";
import PostCard from "./post_card";

/**
 * Subclass of PostCard that is responsible for the creation of a group post.
 */
class NewGroupPost extends PostCard {

    /**
     * The class constructor that receives a user_id and a group_id. Binds the create_new_post method.
     */
    constructor(props) {
        super(props);
        this.state = {
            user_id: props.id,
            group_id: props.group_id,
        };
        this.create_new_post = this.create_new_post.bind(this);
    }

    /**
     * Sends a POST request to the backend for the creation of a group post.
     */
    create_new_post() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                "message": this.state.message,
                "author": `/api/users/${this.state.user_id}`,
                "myGroup": `/api/myGroups/${this.state.group_id}`
            })
        };

        fetch('/api/posts', requestOptions)
            .then(response => {
                if (response.status === 201) {
                    window.location.reload();
                }
            })
            .catch(error => console.error('There was an error!', error));
    }
}

export default NewGroupPost;