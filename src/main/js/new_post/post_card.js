import React from "react";
import {Button, Card, CardBody, CardText, CardTitle, Input} from 'reactstrap';

/**
 * Class that represents a post. The create_new_post method is empty.
 */
class PostCard extends React.Component {

    /**
     * The constructor of the class is responsible for the initialization of the message in the state.
     */
    constructor(props) {
        super(props);
        this.state = {
            message: "",
        };
        this.create_new_post = this.create_new_post.bind(this);
    }

    /**
     * Empty method that represents a post creation.
     */
    create_new_post() {
    }

    /**
     * Shadow style
     * @type {{boxShadow: string, WebkitBoxShadow: string, MozBoxShadow: string}}
     */
    shadow_style = {
        WebkitBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        MozBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        boxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        width: "70%"
    }

    /**
     * Generates the JSX that represent the actual post.
     * @returns {JSX.Element}
     */
    render() {
        return (
            <div className="d-flex justify-content-around">
                <div className="col-md-8">

                    <Card className="m-3 mx-auto" body outline style={this.shadow_style}>
                        <CardBody>
                            <CardTitle>
                                <h5 className="d-inline-block">Add new post</h5>
                            </CardTitle>
                            <CardText>
                                <Input
                                    id="message-input"
                                    name="text"
                                    type="textarea"
                                    value={this.state.message}
                                    onChange={(e) => {
                                        this.setState({message: e.target.value})
                                    }}
                                />
                            </CardText>
                            <Button onClick={this.create_new_post}
                                    style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                                    className="float-end"
                                    id="add-post"
                                    size="lg">
                                Post
                            </Button>
                        </CardBody>
                    </Card>
                </div>
            </div>
        )
    }

}

export default PostCard;