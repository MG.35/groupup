import React from "react";
import {Button} from 'reactstrap';

/**
 * This component is responsible for displaying the "Join Group" or "Leave Group" button.
 * @returns {JSX.Element}
 */
export default function JoinGroupButton(props) {

    /**
     * This state is responsible to save the users joined in a group
     */
    const [usersList, setUsersList] = React.useState([]);

    /**
     * Get the users for this group
     */
    const getUsers = () => {
        return fetch(`/api/myGroups/${props.groupid}/users`)
            .then(response => response.json())
            .then(data => data._embedded.users)
            .then(users => users.map(user => user._links.self.href))
    }


    /**
     * use effect to get the users for this group and save it in the state
     */
    React.useEffect(() => {
        getUsers()
            .then(data => setUsersList(data))
        ;
    }, []);

    /**
     * Triggered when "Leave Group" is clicked.
     */
    function leaveGroup() {
        let requestBody = "";
        let newUsersList = usersList.filter(line => parseInt(line.split("/")[line.split("/").length - 1]) !== parseInt(props.userid));
        newUsersList.forEach(line => requestBody += (line + "\n"))

        const xhr = new XMLHttpRequest();
        xhr.open("PUT", `/api/myGroups/${props.groupid}/users`);
        xhr.setRequestHeader("Content-Type", "text/uri-list");
        xhr.send(requestBody);
        xhr.onload = function () {
            const status = xhr.status
            if (status === 204) {
                window.location.reload();
            }
        }
    }

    /**
     * Triggered when "Join Group" is clicked.
     */
    function joinGroup() {
        let requestBody = "";
        usersList.forEach(line => requestBody += (line + "\n"))
        requestBody += window.location.href.split("/")[0] + "//" + window.location.href.split("/")[2] + `/api/users/${props.userid}`

        const xhr = new XMLHttpRequest();
        xhr.open("PUT", `/api/myGroups/${props.groupid}/users`);
        xhr.setRequestHeader("Content-Type", "text/uri-list");
        xhr.send(requestBody);
        xhr.onload = function () {
            const status = xhr.status
            if (status === 204) {
                window.location.reload();
            }
        }
    }


    /**
     * This function is responsible to display the "Join Group" or "Leave Group" button.
     * @returns {JSX.Element}
     */
    if (
        usersList.includes(window.location.href.split("/")[0] + "//" + window.location.href.split("/")[2] + `/api/users/${props.userid}`)
    ) {
        return (
            <div className="mx-auto">
                <Button onClick={leaveGroup} style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                        id="leave-group">Leave Group</Button>
            </div>
        );
    } else {
        return (
            <div className="mx-auto">
                <Button onClick={joinGroup} style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                        id="join-group">Join Group</Button>
            </div>
        )
    }
}
