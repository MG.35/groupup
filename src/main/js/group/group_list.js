import React from "react";
import {useContext} from "react";
import UserContext from "../shared/user-context/user_context";
import MyGroups from "./myGroups";
import {Button} from "reactstrap";
import {Link} from "react-router-dom";
import Navbar from "../navigation/navbar/navbar";

/**
 * This component is responsible for displaying the page with my groups list.
 * @returns {JSX.Element}
 */
export default function GroupList() {

    /**
     * This hook is responsible for getting the groups of the user.
     */
    const [myGroups, setGroups] = React.useState([]);

    /**
     * @user represents the current logged user.
     */
    let user = useContext(UserContext);

    /**
     * Get the groups of the user
     * @returns {Promise<any>}
     */
    function get_groups() {
        return fetch(`/api/users/${user.id}/myGroups`)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * Call the get_groups functions and set the state of the component
     */
    React.useEffect(() => {
        get_groups()
            .then(data => setGroups(data._embedded.myGroups.map(group => {
                if (group.photo == null || group.photo === "") {
                    group.photo = "/default-group.png"
                }
                return group;
            })))
        ;
    }, [!myGroups]);


    /**
     * Render the component
     */
    return (
        <>
            <Navbar/>
            <div id="group-list">
                <MyGroups myGroups={myGroups}/>
            </div>
            <div className={"d-flex justify-content-evenly"}>
                <Link to="/group/create" style={{textDecoration: 'none'}} className="mt-5">
                    <Button
                        style={{
                            background: "#03CCAC",
                            borderColor: "#03CCAC",
                            color: "white",
                            width: "50px",
                            height: "50px",
                            display: "block",
                            borderRadius: "125px",
                            WebkitBorderRadius: "125px",
                            MozBorderRadius: "125px",
                        }}
                        id="create-group">
                        +
                    </Button></Link>
            </div>
        </>
    )
}