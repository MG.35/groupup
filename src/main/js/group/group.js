import React, {useContext} from "react";
import {useParams} from "react-router-dom";
import Posts from "../shared/post/posts.js";
import {Information} from "../shared/information/information";
import UserContext from "../shared/user-context/user_context";
import Navbar from "../navigation/navbar/navbar";
import NewGroupPost from "../new_post/new_post_group";

/**
 * This component is responsible for displaying the section to view a group's page
 * @returns {JSX.Element}
 */
function Group() {

    /**
     * This user is getted by the UserContext, and references the current logged user
     */
    const user = useContext(UserContext);

    /**
     * This id is the id of the group, set as url param
     */
    const id = useParams().id;

    /**
     * This hook is responsible for getting information of the group
     */
    const [group, setGroup] = React.useState([]);

    /**
     * This hook is responsible for getting the posts of the group
     */
    const [posts, setPosts] = React.useState([]);

    /**
     * HTTP request to get a resource
     * @param url
     * @returns {Promise<any>}
     */
    function get_data(url) {
        return fetch(url)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * This function is used to get the group's information and the group's posts,
     * and is called when the component is mounted or author state is updated
     */
    React.useEffect(
        () => {
            get_data(`/api/myGroups/${id}`)
                .then(data => {
                        if (data.photo == null || data.photo === "") {
                            data.photo = "/default-group.png"
                        }
                        setGroup(data);
                        if (data?._links?.posts?.href) {
                            get_data(data._links.posts.href)
                                .then(data => {
                                        return setPosts(data._embedded.posts);
                                    }
                                );
                        }
                    }
                );
        }, [!group]
    )

    /**
     * Render the information and the posts of the group
     */
    return (
        <div>
            <Navbar/>
            <div id="group">
                <Information name={group.name} groupid={id} userid={user.id} photo={group.photo}/>
                <NewGroupPost id={user.id} group_id={id}/>
                <Posts posts={posts}/>
            </div>
        </div>
    );
}

export default Group;