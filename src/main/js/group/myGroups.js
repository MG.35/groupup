import React from "react";

import {Button, Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";

/**
 * This component is responsible for rendering the list my groups.
 * @param props
 * @returns {JSX.Element}
 */
function MyGroups(props) {

    /**
     * Shadow style
     * @type {{boxShadow: string, WebkitBoxShadow: string, MozBoxShadow: string}}
     */
    const shadow_style = {
        WebkitBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        MozBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        boxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        width: "60%"
    }

    /**
     * Style for rounded image
     * @type {{WebkitBorderRadius: string, borderRadius: string, MozBorderRadius: string, display: string, width: string, backgroundSize: string, height: string}}
     */
    const round_image = {
        width: "90px",
        height: "90px",
        backgroundSize: "cover",
        display: "block",
        borderRadius: "125px",
        WebkitBorderRadius: "125px",
        MozBorderRadius: "125px",
    }


    /**
     * This function is responsible for rendering the list of my groups.
     * @param myGroups
     * @returns {JSX.Element}
     */
    function groups_info(myGroups) {
        return myGroups.map((group, index) => {
            const keyBaseGroup = "group";
            let groupId = group._links.self.href.split("/").pop();
            return (
                <Card key={keyBaseGroup + index.toString()} className="m-3 mx-auto" body style={shadow_style} outline>
                    <CardBody>
                        <img id={"photo" + groupId} style={round_image} src={group.photo}
                             className="d-inline-block me-3" alt="group-image"/>
                        <h5 id={"group-name-" + groupId} className="d-inline-block">{group.name}</h5>

                        <Link
                            to={"/group/" + group._links.self.href.split("/")[group._links.self.href.split("/").length - 1]}><Button
                            style={{background: "white", borderColor: "#03CCAC", color: "#03CCAC"}}
                            className="float-end mt-4"
                            id="add-post"
                            size="lg">Open</Button></Link>
                    </CardBody>
                </Card>
            )
        });
    }

    /**
     * Render the list my groups as a list of groups info (name and description).
     */
    return (
        <div className="d-flex justify-content-around">
            <div id="my-groups" className="col-md-8">
                {groups_info(props.myGroups)}
            </div>
        </div>
    );
}

export default MyGroups;
