import React from "react";
import {Button, Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";

/**
 * This component is responsible for rendering a single saved labels
 * @returns {JSX.Element}
 */
export default function SavedLabel(props) {

    /**
     * Triggered when "Delete Label" is clicked. Deletes the label.
     */
    function deleteLabels() {
        fetch(props.label._links.self.href, {method: "DELETE"}).then(() => window.location.reload());
    }


    /**
     * Shadow style
     * @type {{boxShadow: string, WebkitBoxShadow: string, MozBoxShadow: string}}
     */
    const shadow_style = {
        WebkitBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        MozBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        boxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        width: "60%"
    }

    /**
     * Render the component
     */
    return (
        <>
            <Card id={"label-" + props.label.label} className="m-3 mx-auto" body style={shadow_style} outline>
                <CardBody>
                    <h5 className="d-inline-block mt-2">{props.label.label}</h5>
                    <Button
                        style={{background: "white", borderColor: "#03CCAC", color: "#03CCAC"}}
                        onClick={deleteLabels}
                        className="delete-label float-end ms-2"
                        id="delete-label"
                        size="lg">Delete</Button>
                    <Link
                        to={"/saved-posts/" + props.label.label}><Button
                        style={{background: "white", borderColor: "#03CCAC", color: "#03CCAC"}}
                        className="float-end ms-auto"
                        id="posts-with-label-button"
                        size="lg">Open</Button></Link>
                </CardBody>
            </Card>

        </>
    );

}
