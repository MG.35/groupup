import React, {useContext} from "react";
import Navbar from "../navigation/navbar/navbar";
import UserContext from "../shared/user-context/user_context";
import SavedLabel from "./saved_label";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";

/**
 * This component is responsible for rendering the saved labels
 * @returns {JSX.Element}
 */
export default function SavedLabels() {

    /**
     * This hook is responsible for getting the saved labels of the user.
     */
    const [savedLabels, setSavedLabels] = React.useState([]);


    /**
     * This function is responsible for rendering the list of labels.
     * @param labels
     * @returns {JSX.Element}
     */
    function labels_list(labels) {
        return labels.map((label, index) => {
            return (
                <SavedLabel key={index} label={label}/>
            )
        });
    }

    /**
     * Return logged-in user
     */
    let user = useContext(UserContext);

    /**
     * Get the user's saved labels
     * @returns {Promise<any>}
     */
    function get_saved_labels() {
        return fetch(`/api/users/${user.id}/myLabels`)
            .then(response => response.json())
            .then(data => {
                return data
            })
    }

    /**
     * Call the get_saved_labels function and set the state of the component
     */
    React.useEffect(() => {
        get_saved_labels()
            .then(data => setSavedLabels(data._embedded.myLabels))
        ;
    }, []);


    /**
     * Render the component
     */
    return (
        <>
            <Navbar/>
            <div className="mt-3 mx-auto mb-3" style={{width: "190px"}}>
                <h1>My labels</h1>
            </div>
            <div className="d-flex justify-content-around mb-3">
                <div className="col-md-8" id="labels">
                    {labels_list(savedLabels)}
                </div>
            </div>
            <div className={"d-flex justify-content-evenly"}>
                <Link to="/label/create" style={{textDecoration: 'none'}} className="mt-5">
                    <Button
                        style={{
                            background: "#03CCAC",
                            borderColor: "#03CCAC",
                            color: "white",
                            width: "50px",
                            height: "50px",
                            display: "block",
                            borderRadius: "125px",
                            WebkitBorderRadius: "125px",
                            MozBorderRadius: "125px",
                        }}
                        id="create-label">
                        +
                    </Button></Link>
            </div>
        </>
    );
}
