import React, {useContext} from "react";
import UserContext from "../shared/user-context/user_context";
import Navbar from "../navigation/navbar/navbar";
import {useParams} from "react-router-dom";
import SavedPost from "./saved_post";


/**
 * This component is responsible for rendering the saved posts with a given label.
 * @returns {JSX.Element}
 */
export default function SavedPosts() {

    const label = useParams().label;

    /**
     * Return logged-in user
     */
    let user = useContext(UserContext);

    /**
     * This hook is responsible for saving the posts with the given label.
     */
    const [savedPosts, setSavedPosts] = React.useState([]);

    /**
     * Get the user's saved labels
     * @returns {Promise<any>}
     */
    function get_saved_labels() {
        return fetch(`/api/users/${user.id}/myLabels`)
            .then(response => response.json())
            .then(data => {
                return data
            })
    }

    /**
     * HTTP request to get the saved posts of the user with the given label
     * @returns {Promise<any>}
     */
    function get_post_with_label(url) {
        return fetch(url)
            .then(response => response.json())
            .then(data => {
                return data
            })
    }

    /**
     * get the saved posts of the user with the given label
     */
    React.useEffect(() => {
        get_saved_labels()
            .then(data => data._embedded.myLabels)
            .then(labels => labels.filter(this_label => this_label.label === label))
            .then(filteredLabel => get_post_with_label(filteredLabel[0]._links.savedPosts.href))
            .then(data => setSavedPosts(data._embedded.savedPosts))
    }, [label]);


    /**
     * This function is responsible for rendering the list of posts
     * @param posts
     * @returns {JSX.Element}
     */
    function posts_list(posts) {
        return posts.map((post, index) => {
            return (
                <SavedPost key={index} post={post}/>
            )
        });
    }

    /**
     * Render the component
     */
    return (
        <>
            <Navbar/>
            <div className="mt-3 mx-auto mb-3" style={{width: "380px"}}>
                <h1>{label}'s posts</h1>
            </div>
            <div className="d-flex justify-content-around mb-3">
                <div className="col-md-8" id="posts-label">
                    {posts_list(savedPosts)}
                </div>
            </div>
        </>);
}