import React from "react";
import Post from "../shared/post/post";
import {useParams} from "react-router-dom";


/**
 * This component is responsible for rendering a single saved post
 * @returns {JSX.Element}
 */
export default function SavedPost(props) {
    const label = useParams().label;


    /**
     * This function is used to remove the post from the label
     */
    const removeSavedPost = () => {
        fetch(`${props.post._links.self.href}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            window.location.reload();
        });
    }
    /**
     * This function is responsible for deleting a post.
     * @param url The url of the post to be deleted.
     */
    function removePost(url) {
        const requestOptions = {method: 'DELETE'};

        fetch(url, requestOptions)
            .then(() => {
                window.location.reload();
            })
            .catch(error => console.error('There was an error in deleting the post', error));
    }

    /**
     * This state is used to store the post to be rendered
     */
    const [post, setPost] = React.useState([]);

    /**
     * HTTP request to get the post
     * @returns {Promise<any>}
     */
    function get_post() {
        return fetch(props.post._links.post.href)
            .then(response => response.json())
            .then(data => {
                return data
            })
    }

    /**
     * call get_post() and set the post state
     */
    React.useEffect(() => {
        get_post()
            .then(data => setPost(data))
    }, []);


    /**
     * render the component
     */
    return (
        <div>
            { // check if there is _links in the post object
                post._links ?
                    <div>
                        <Post post={post} removeLabel={removeSavedPost} removePost={removePost} label={label}/>
                    </div>
                    :
                    <div>
                    </div>
            }
        </div>)
}