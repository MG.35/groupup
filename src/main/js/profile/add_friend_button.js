import React from "react";
import {Button} from 'reactstrap';

/**
 * This component is responsible for displaying the "Add Friend" or "Remove Friend" button.
 * @returns {JSX.Element}
 */
export default function AddFriendButton(props) {

    const [friendsList, setFriendsList] = React.useState([]);

    /**
     * Get the friends for this user
     */
    function getFriends() {
        return fetch(`/api/users/${props.userid}/friends`)
            .then(response => response.json())
            .then(data => data._embedded.users)
            .then(users => {
                return users
            })
    }


    /**
     * Get the friends for this user and save them to the state
     */
    React.useEffect(() => {
        getFriends()
            .then(data => setFriendsList(data.map(user => user._links.self.href)))
        ;
    }, []);

    /**
     * Triggered when "Remove Friend" is clicked.
     */
    function removeFriend() {
        let requestBody = "";
        let newFriendsList = friendsList.filter(line => parseInt(line.split("/")[line.split("/").length - 1]) !== parseInt(props.currentuserid));
        newFriendsList.forEach(line => requestBody += (line + "\n"))

        const xhr = new XMLHttpRequest();
        xhr.open("PUT", `/api/users/${props.userid}/friends`);
        xhr.setRequestHeader("Content-Type", "text/uri-list");
        xhr.send(requestBody);
        xhr.onload = function () {
            const status = xhr.status
            if (status === 204) {
                window.location.reload();
            }
        }
    }

    /**
     * Triggered when "Add Friend" is clicked.
     */
    function addFriend() {
        let requestBody = "";
        friendsList.forEach(line => requestBody += (line + "\n"))
        requestBody += window.location.href.split("/")[0] + "//" + window.location.href.split("/")[2] + `/api/users/${props.currentuserid}`

        const xhr = new XMLHttpRequest();
        xhr.open("PUT", `/api/users/${props.userid}/friends`);
        xhr.setRequestHeader("Content-Type", "text/uri-list");
        xhr.send(requestBody);
        xhr.onload = function () {
            const status = xhr.status
            if (status === 204) {
                window.location.reload();
            }
        }
    }


    /**
     * Determine if the logged user is not the same as the user being viewed.
     */
    if (parseInt(props.userid) === parseInt(props.currentuserid)) {
        return null;
    }

    /**
     * Determine if the logged user is already a friend of the user being viewed.
     */
    if (
        friendsList.includes(window.location.href.split("/")[0] + "//" + window.location.href.split("/")[2] + `/api/users/${props.currentuserid}`)
    ) {
        return (
            <div className="mx-auto">
                <Button onClick={removeFriend}
                        style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                        id="remove-friend">Remove Friend
                </Button>
            </div>
        );
    } else {
        return (
            <div className="mx-auto">
                <Button onClick={addFriend}
                        style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                        id="add-friend">Add Friend</Button>
            </div>
        )
    }
}