import React from "react";
import {useParams} from "react-router-dom";
import {Information} from "../shared/information/information"
import Posts from "../shared/post/posts"
import UserContext from "../shared/user-context/user_context";
import NewUserPost from "../new_post/new_post_user";
import Navbar from "../navigation/navbar/navbar";

/**
 * This component is responsible for displaying the profile page.
 * @returns {JSX.Element}
 */
function Profile() {

    /**
     * Return logged-in user
     */
    let loggedUser = React.useContext(UserContext);

    /**
     * This hook is responsible for getting the posts of the user.
     */
    const [posts, setPosts] = React.useState([]);
    /**
     * This hook is responsible for getting the information of the user.
     */
    const [user, setUser] = React.useState([])
    /**
     * The useParams hook is responsible for getting the id of the user.
     */
    const id = useParams().id;


    /**
     * Get the posts of the user
     * @returns {Promise<any>}
     */
    function get_posts() {
        return fetch(`/api/users/${id}/posts`)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * Get the user information
     * @returns {Promise<any>}
     */
    function get_user() {
        return fetch(`/api/users/${id}`)
            .then(response => response.json())
            .then(data => {
                return data
            })
    }

    /**
     * Call the get_posts and get_user functions and set the state of the component
     */
    React.useEffect(() => {
        get_posts()
            .then(data => setPosts(data._embedded.posts))
        ;
        get_user()
            .then(data => setUser(data))
        ;


    }, []);

    /**
     * Render the component
     */
    return (
        <div>
            <Navbar/>
            <div id="profile">
                <Information name={user.name + " " + user.surname} photo={user.photo} username={user.username}
                             userid={loggedUser.id} currentuserid={id}/>
                {(parseInt(loggedUser.id) === parseInt(id)) && <NewUserPost id={id}/>}
                <div id="personal-posts">
                    <Posts posts={posts}/>
                </div>
            </div>
        </div>
    )
}

export default Profile;