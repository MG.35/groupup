import React from "react";
import {useState, useContext} from "react";
import {Button, Card, CardBody, CardText, CardTitle, Input} from 'reactstrap';
import UserContext from "../shared/user-context/user_context";
import {useNavigate} from 'react-router-dom';
import Navbar from "../navigation/navbar/navbar";


/**
 * This component is responsible for displaying the card for the label creation
 * @returns {JSX.Element}
 */
export default function LabelCreate() {

    /**
     * This hook is responsible for getting the name of the new label
     */
    const [label, setLabel] = useState("");


    /**
     * This user is getted by the UserContext, and references the current logged user
     */
    let userc = useContext(UserContext);

    /**
     * This is a function which allows navigating in the app
     */
    const navigate = useNavigate();

    /**
     * POST request to the backend
     */
    function clicked() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                label,
                user: '/api/users/' + userc.id
            })
        };
        fetch('/api/labels', requestOptions)
            .then(response => response.json())
            .then(data => navigate('/saved-posts'))
            .catch(error => console.error('There was an error!', error));
    }

    /**
     * Shadow style
     * @type {{boxShadow: string, WebkitBoxShadow: string, MozBoxShadow: string}}
     */
    const shadow_style = {
        WebkitBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        MozBoxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        boxShadow: "-5px 5px 50px -23px rgba(0, 0, 0, 0.75)",
        width: "70%"
    }

    /**
     * Generate the card for the group creation
     * @return JSX object
     */
    return (
        <div>
            <Navbar/>

            <div className="d-flex justify-content-around">
                <div className="col-md-8">
                    <Card className="m-3 mx-auto" body outline style={shadow_style}>
                        <CardBody>
                            <CardTitle>
                                <h5 className="d-inline-block">Create Label</h5>
                            </CardTitle>
                            <CardText>
                                <Input
                                    id="label_id"
                                    label="label"
                                    type="textarea"
                                    placeholder="Name of the label.."
                                    value={label}
                                    onChange={(e) => {
                                        setLabel(e.target.value)
                                    }}
                                />
                            </CardText>

                            <Button onClick={() => navigate('/saved-posts')}
                                    style={{background: "white", borderColor: "#03CCAC", color: "#03CCAC"}}>
                                Cancel
                            </Button>
                            <Button onClick={clicked} color="primary" id="button-c"
                                    style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                                    className="float-end"
                            >
                                Create
                            </Button>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </div>
    )
}
