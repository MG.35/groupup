import React from "react";


/**
 * Home page
 * @returns {JSX.Element}
 */
export default function Home() {
    /**
     * Style for homepage
     * @type {{height: string}}
     */
    const style = {
        height: "100vh",
    }
    /**
     * Style for the first column
     * @type {{boxShadow: string, WebkitBoxShadow: string, backgroundImage: string, MozBoxShadow: string, backgroundSize: string, height: string}}
     */
    const boxShadow = {
        WebkitBoxShadow: "inset -16px 0px 45px -42px rgba(51,51,51,1)",
        MozBoxShadow: "inset -16px 0px 45px -42px rgba(51,51,51,1)",
        boxShadow: "inset -16px 0px 45px -42px rgba(51,51,51,1)",
        height: "100vh",
        backgroundImage: 'url("/home-background.jpg")',
        backgroundSize: "cover"
    }

    /**
     * Render the page
     */
    return (
        <div className="d-flex flex-row align-items-center" style={style}>
            <div className="col-md-5" style={boxShadow}>

            </div>
            <div className="col-md-7 ps-5 pe-5 my-auto">
                <h1 style={{color: "#FC486B"}}>
                    GROUP UP
                </h1>
                <h3 style={{color: "#333333", fontWeight: 400}}>
                    Join the community that helps you get connected.
                </h3>
                <a id="sign-in-button"
                   className="btn w-100 me-5"
                   style={{background: "#03CCAC", border: "#03CCAC", color: "white"}}
                   href="/board"
                   role="button"
                >
                    Sign in

                </a>
            </div>

        </div>

    )
}