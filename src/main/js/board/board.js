import React from "react";
import Posts from "../shared/post/posts";
import {useContext} from "react";
import UserContext from "../shared/user-context/user_context";
import Navbar from "../navigation/navbar/navbar";

/**
 * This component is responsible for displaying my friends comments.
 * @returns {JSX.Element}
 */
export default function Board() {

    /**
     * This hook is responsible for getting the posts of the user.
     */
    const [posts, setPosts] = React.useState([]);

    /**
     * @user represents the current logged user.
     */
    let user = useContext(UserContext);

    /**
     * Get the posts of my friends
     * @returns {Promise<any>}
     */
    function get_posts() {
        return fetch(`/api/posts/search/board-posts?userId=${user.id}`)
            .then(response => response.json())
            .then(data => {
                return data;
            })
    }

    /**
     * Call the get_posts functions and set the state of the component
     */
    React.useEffect(() => {
        get_posts()
            .then(data => setPosts(data._embedded.posts))
        ;
    }, [!posts]);

    /**
     * Render the component
     */
    return (
        <div>
            <Navbar/>
            <div id="all-posts">
                <Posts posts={posts}/>
            </div>
        </div>
    )
}