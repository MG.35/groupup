package it.unimib.groupup.controller;

/**
 * This class is used to wrap the logged user's information.
 */
public class WhoAmI {

    /**
     * The logged user's id.
     */
    private int id;

    /**
     * The logged user's name.
     */
    private String name;

    /**
     * The logged user's surname.
     */
    private String surname;

    /**
     * The logged user's email.
     */
    private String email;

    /**
     * The logged user's username.
     */
    private String username;

    /**
     * The logged user's photo.
     */
    private String photo;


    /**
     * Empty constructor.
     */
    public WhoAmI() {
    }

    /**
     * This method returns the logged user's id.
     * @return the logged user's id.
     */
    public int getId() {
        return id;
    }

    /**
     * This method sets the logged user's id.
     * @param id the logged user's id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This method returns the logged user's name.
     * @return the logged user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * This method sets the logged user's name.
     * @param name the logged user's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method returns the logged user's surname.
     * @return the logged user's surname.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * This method sets the logged user's surname.
     * @param surname the logged user's surname.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * This method returns the logged user's email.
     * @return the logged user's email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * This method sets the logged user's email.
     * @param email the logged user's email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This method returns the logged user's username.
     * @return the logged user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method sets the logged user's username.
     * @param username the logged user's username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * This method returns the logged user's photo.
     * @return the logged user's photo.
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * This method sets the logged user's photo.
     * @param photo the logged user's photo.
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
