package it.unimib.groupup.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * This controller manages the routes for the web UI.
 * It's strongly integrated with the React js frontend.
 */
@Controller
public class MainController {

    /**
     * Manages the UI of the home page
     */
    @RequestMapping(value = "/")
    public String home() {
        return "index";
    }

    /**
     * Manages the UI of the board
     */
    @RequestMapping(value = "/board")
    public String board() { return "index"; }

    /**
     * Manages the UI of a user profile
     */
    @RequestMapping(value = "/profile/{username}")
    public String profile(@PathVariable("username") String username) {
        return "index";
    }

    /**
     * Manages the UI of a specific group
     */
    @RequestMapping(value = "/group/{id}")
    public String group(@PathVariable("id") Integer id) {
        return "index";
    }

    /**
     * Manages the UI of the groups list
     */
    @RequestMapping(value = "/group/list")
    public String groupList() { return "index"; }

    /**
     * Manages the UI of the group creation
     */
    @RequestMapping(value = "/group/create")
    public String groupCreate() { return "index"; }

    /**
     * Manages the UI of the label creation
     */
    @RequestMapping(value = "/label/create")
    public String labelCreate() { return "index"; }
    /**
     * Manages the UI of the saved posts for a user
     */
    @RequestMapping(value = "/saved-posts")
    public String savedPosts() { return "index"; }

    /**
     * Manages the UI of the saved posts for a user given a label
     */
    @RequestMapping(value = "/saved-posts/{label}")
    public String group(@PathVariable("label") String label) {
        return "index";
    }

}
