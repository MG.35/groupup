package it.unimib.groupup.controller;

import it.unimib.groupup.repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * This class is used to get the current user logged in
 */
@RestController
public class ContextController {

    /**
     * This repository is used to get the current user logged in
     */
    private final UserRepository userRepository;

    /**
     * Constructor
     * @param userRepository the user repository
     */
    public ContextController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Define the get mapping to /whaami endpoint to get the current user logged in
     * @return a json with the current user logged in
     */
    @GetMapping("/whoami")
    public WhoAmI index() {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Integer id = userRepository.findByUsername(username).getId();
        String name = userRepository.findByUsername(username).getName();
        String surname = userRepository.findByUsername(username).getSurname();
        String email = userRepository.findByUsername(username).getEmail();
        String photo = userRepository.findByUsername(username).getPhoto();


        WhoAmI whoAmI = new WhoAmI();
        whoAmI.setId(id);
        whoAmI.setName(name);
        whoAmI.setSurname(surname);
        whoAmI.setEmail(email);
        whoAmI.setPhoto(photo);
        whoAmI.setUsername(username);


        return whoAmI;
    }



}
