package it.unimib.groupup;

import it.unimib.groupup.model.*;
import it.unimib.groupup.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Fake database for manual testing
 */
@Component
public class FakeDatabase implements CommandLineRunner {

    /**
     * Repositories for CRUD operations
     */
    final private MyGroupRepository myGroupRepository;
    final private UserRepository userRepository;
    final private PostRepository postRepository;
    final private CommentRootRepository commentRootRepository;
    final private CommentLeafRepository commentLeafRepository;
    final private SavedPostRepository savedPostRepository;
    final private MyLabelsRepository myLabelsRepository;

    /**
     * Spring will automatically inject the dependency in the constructor
     *
     * @param myGroupRepository
     * @param userRepository
     * @param postRepository
     * @param commentRootRepository
     * @param commentLeafRepository
     * @param savedPostRepository
     * @param myLabelsRepository
     */
    @Autowired
    public FakeDatabase(MyGroupRepository myGroupRepository, UserRepository userRepository, PostRepository postRepository, CommentRootRepository commentRootRepository, CommentLeafRepository commentLeafRepository, SavedPostRepository savedPostRepository, MyLabelsRepository myLabelsRepository) {
        this.myGroupRepository = myGroupRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.commentRootRepository = commentRootRepository;
        this.commentLeafRepository = commentLeafRepository;
        this.savedPostRepository = savedPostRepository;
        this.myLabelsRepository = myLabelsRepository;
    }

    /**
     * Method invoked when starting the application
     */
    @Override
    public void run(String... strings) throws Exception {

        // create some users
        User user1 = new User(
                "Willis",
                "Lyon",
                "w.lyon@domain.com",
                "password",
                "frances",
                "/user1.jpg");
        User user2 = new User(
                "Dell",
                "Coburn",
                "d.coburn@domain.com",
                "password",
                "ivy",
                "/user2.jpg");
        User user3 = new User(
                "Duncan",
                "Vernon",
                "d.vernon@domain.com",
                "password",
                "watt",
                "/user3.jpg");

        // save the users
        this.userRepository.save(user1);
        this.userRepository.save(user2);
        this.userRepository.save(user3);


        // create a set the users in order to create a group
        Set<User> users = new java.util.HashSet<>();
        users.add(user1);
        users.add(user3);

        // create group
        MyGroup group = new MyGroup("Motorsport", "A group for motorsport lovers");
        // add users to group
        group.setUsers(users);
        group.setPhoto("/group.jpg");
        // save the group
        this.myGroupRepository.save(group);

        // create some posts
        Post post1 = new Post("It doesn't sound like that will ever be on my travel list.");
        Post post2 = new Post("Hang on, my kittens are scratching at the bathtub and they'll upset by the lack of biscuits.");
        Post post3 = new Post("Plans for this weekend include turning wine into water.");
        Post post4 = new Post("I don’t respect anybody who can’t tell the difference between Pepsi and Coke.");
        Post post5 = new Post("Not all people who wander are lost.");
        Post post6 = new Post("I made myself a peanut butter sandwich as I didn't want to subsist on veggie crackers.");
        Post post7 = new Post("Tomorrow will bring something new, so leave today as a memory.");
        Post post8 = new Post("Random words in front of other random words create a random sentence.");
        Post post9 = new Post("If I don’t like something, I’ll stay away from it.");
        Post post10 = new Post("My favorite car is the one that has the most doors.");
        Post post11 = new Post("Motorsport is so much fun");


        // add authors to posts
        post1.setAuthor(user1);
        post2.setAuthor(user1);
        post3.setAuthor(user1);
        post10.setAuthor(user1);
        post4.setAuthor(user2);
        post5.setAuthor(user2);
        post6.setAuthor(user2);
        post7.setAuthor(user3);
        post8.setAuthor(user3);
        post9.setAuthor(user3);
        post11.setAuthor(user3);

        // add some likes to a post
        post1.setLikedBy(users);
        post2.setLikedBy(users);

        // add group to posts
        post10.setMyGroup(group);
        post11.setMyGroup(group);

        // save the posts
        this.postRepository.save(post1);
        this.postRepository.save(post2);
        this.postRepository.save(post3);
        this.postRepository.save(post4);
        this.postRepository.save(post5);
        this.postRepository.save(post6);
        this.postRepository.save(post7);
        this.postRepository.save(post8);
        this.postRepository.save(post9);
        this.postRepository.save(post10);
        this.postRepository.save(post11);

        // create some comments root
        CommentRoot commentRoot1 = new CommentRoot("messageCommentRoot1");
        CommentRoot commentRoot2 = new CommentRoot("messageCommentRoot2");
        CommentRoot commentRoot3 = new CommentRoot("messageCommentRoot3");

        // add authors to comments
        commentRoot1.setAuthor(user1);
        commentRoot2.setAuthor(user2);
        commentRoot3.setAuthor(user3);

        // new set of user that like comments
        Set<User> usersLikeCommentRoot1 = new java.util.HashSet<>();
        Set<User> usersLikeCommentRoot2 = new java.util.HashSet<>();
        Set<User> usersLikeCommentRoot3 = new java.util.HashSet<>();


        // add users to set
        usersLikeCommentRoot1.add(user2);
        usersLikeCommentRoot1.add(user3);

        usersLikeCommentRoot2.add(user1);
        usersLikeCommentRoot2.add(user3);

        usersLikeCommentRoot3.add(user1);
        usersLikeCommentRoot3.add(user2);

        // add likes to comment root
        commentRoot1.setLikedBy(usersLikeCommentRoot1);
        commentRoot2.setLikedBy(usersLikeCommentRoot2);
        commentRoot3.setLikedBy(usersLikeCommentRoot3);

        // add comments to posts
        commentRoot1.setPost(post1);
        commentRoot2.setPost(post2);
        commentRoot3.setPost(post3);

        // save the comments
        this.commentRootRepository.save(commentRoot1);
        this.commentRootRepository.save(commentRoot2);
        this.commentRootRepository.save(commentRoot3);

        // create some comments leaf
        CommentLeaf commentLeaf1 = new CommentLeaf("messageCommentLeaf1");
        CommentLeaf commentLeaf2 = new CommentLeaf("messageCommentLeaf2");
        CommentLeaf commentLeaf3 = new CommentLeaf("messageCommentLeaf3");

        // add authors to comments leaf
        commentLeaf1.setAuthor(user3);
        commentLeaf2.setAuthor(user2);
        commentLeaf3.setAuthor(user1);

        // new set of user that like comments leaf
        Set<User> usersLikeCommentLeaf1 = new java.util.HashSet<>();
        Set<User> usersLikeCommentLeaf2 = new java.util.HashSet<>();
        Set<User> usersLikeCommentLeaf3 = new java.util.HashSet<>();

        // add users to set
        usersLikeCommentLeaf1.add(user1);
        usersLikeCommentLeaf1.add(user2);

        usersLikeCommentLeaf2.add(user3);
        usersLikeCommentLeaf2.add(user1);

        usersLikeCommentLeaf3.add(user2);
        usersLikeCommentLeaf3.add(user3);

        // add likes to comment leaf
        commentLeaf1.setLikedBy(usersLikeCommentLeaf1);
        commentLeaf2.setLikedBy(usersLikeCommentLeaf2);
        commentLeaf3.setLikedBy(usersLikeCommentLeaf3);

        // add comments leaf to comments root
        commentLeaf1.setComments(commentRoot1);
        commentLeaf2.setComments(commentRoot2);
        commentLeaf3.setComments(commentRoot3);

        // save the comments leaf
        this.commentLeafRepository.save(commentLeaf1);
        this.commentLeafRepository.save(commentLeaf2);
        this.commentLeafRepository.save(commentLeaf3);

        // add friends
        HashSet<User> friends = new HashSet<>();
        friends.add(user2);
        friends.add(user3);

        // set friends to user
        user1.setFriends(friends);
        this.userRepository.save(user1);

        // create a saved post
        SavedPost savedPost1 = new SavedPost();
        // set the user of the saved post
        savedPost1.setUser(user1);
        // set the post of the saved post
        savedPost1.setPost(post11);
        // create a new label for user1
        MyLabel label1 = new MyLabel();
        label1.setLabel("Motorsport");
        label1.setUser(user1);
        this.myLabelsRepository.save(label1);

        // set the label of the saved post
        savedPost1.setLabel(label1);
        // save the saved post
        this.savedPostRepository.save(savedPost1);


        // create another saved post for user1
        SavedPost savedPost2User1 = new SavedPost();
        // set the user of the saved post
        savedPost2User1.setUser(user1);
        // set the post of the saved post
        savedPost2User1.setPost(post10);
        // set the label of the saved post
        savedPost2User1.setLabel(label1);
        // save the saved post
        this.savedPostRepository.save(savedPost2User1);

        // create another saved post
        SavedPost savedPost2 = new SavedPost();
        // set the user of the saved post
        savedPost2.setUser(user2);
        // set the post of the saved post
        savedPost2.setPost(post11);
        // create a new label for user2
        MyLabel label2 = new MyLabel();
        label2.setLabel("cool");
        label2.setUser(user2);
        this.myLabelsRepository.save(label2);
        // set the label of the saved post
        savedPost2.setLabel(label2);
        // save the saved post
        this.savedPostRepository.save(savedPost2);

    }
}
