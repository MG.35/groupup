package it.unimib.groupup.security;
import it.unimib.groupup.model.User;
import it.unimib.groupup.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * This class is used to load the user details from the database and return them to the security framework.
 */
@Component
public class SpringDataJpaUserDetailsService implements UserDetailsService {

    /**
     * The user repository to get the user from the database.
     */
    private final UserRepository userRepository;

    /**
     * The constructor of the class.
     * @param userRepository the user repository to get the user from the database.
     */
    @Autowired
    public SpringDataJpaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method is used to load the user from the database and return it to the security framework.
     * @param username the username of the user to load.
     * @return the user details in the form of a UserDetails object to be used by the security framework.
     * @throws UsernameNotFoundException if the user is not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                AuthorityUtils.createAuthorityList(user.getRoles()));
    }

}