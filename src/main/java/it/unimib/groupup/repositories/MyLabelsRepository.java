package it.unimib.groupup.repositories;

import it.unimib.groupup.model.MyLabel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * CRUD Repository of labels
 */

@RepositoryRestResource(path = "labels")
public interface MyLabelsRepository extends CrudRepository<MyLabel, Integer> {

    /**
     * Returns label of a post seen by a user
     * @param userId - the id of the user
     * @param postId - the id of the post
     */
    @RestResource(path = "post-label", rel = "post-label")
    @Query("""
            SELECT l FROM MyLabel l 
            JOIN SavedPost sp ON l = sp.label
            WHERE sp.post.id = :postId AND sp.user.id = :userId
            """)
    MyLabel getPostLabel(@Param("userId") Integer userId, @Param("postId") Integer postId);

}