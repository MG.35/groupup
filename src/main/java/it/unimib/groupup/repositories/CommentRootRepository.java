package it.unimib.groupup.repositories;

import it.unimib.groupup.model.CommentRoot;
import org.springframework.data.repository.CrudRepository;

/**
 * CRUD Repository of the root comments
 */
public interface CommentRootRepository extends CrudRepository<CommentRoot, Integer> {
}