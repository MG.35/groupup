package it.unimib.groupup.repositories;

import it.unimib.groupup.model.SavedPost;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * CRUD Repository of saved posts
 */
public interface SavedPostRepository extends CrudRepository<SavedPost, Integer> {


    /**
     * Returns saved post of a post seen by a user
     * @param userId - the id of the user
     * @param postId - the id of the post
     */
    @RestResource(path = "saved-post", rel = "saved-post")
    @Query("""
            SELECT sp FROM SavedPost sp 
            WHERE sp.post.id = :postId AND sp.user.id = :userId
            """)
    SavedPost getPostSavedPost(@Param("userId") Integer userId, @Param("postId") Integer postId);

}