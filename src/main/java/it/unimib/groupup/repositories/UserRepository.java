package it.unimib.groupup.repositories;

import it.unimib.groupup.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * CRUD Repository of the users
 */
public interface UserRepository extends CrudRepository<User, Integer> {

    /**
     * Find a user by its username
     * @param username the username of the user
     * @return the user
     */
    User findByUsername(String username);
}