package it.unimib.groupup.repositories;

import it.unimib.groupup.model.MyGroup;
import org.springframework.data.repository.CrudRepository;

/**
 * CRUD Repository of groups
 */
public interface MyGroupRepository extends CrudRepository<MyGroup, Integer> {
}