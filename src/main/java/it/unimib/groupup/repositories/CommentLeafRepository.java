package it.unimib.groupup.repositories;

import it.unimib.groupup.model.CommentLeaf;
import org.springframework.data.repository.CrudRepository;

/**
 * CRUD Repository of the leaf comments
 */
public interface CommentLeafRepository extends CrudRepository<CommentLeaf, Integer> {
}