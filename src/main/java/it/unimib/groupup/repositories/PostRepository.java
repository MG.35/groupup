package it.unimib.groupup.repositories;

import it.unimib.groupup.model.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * CRUD Repository of the posts
 */
public interface PostRepository extends CrudRepository<Post, Integer> {

    /**
     * Returns board posts of the user (post of the group he is joined, and users he follows) ordered by date (descend)
     * @param userId - the id of the user
     */
    @RestResource(path = "board-posts", rel = "board-posts")
    @Query("""
            SELECT p FROM Post p 
            WHERE p.id IN ( SELECT pf.id FROM User u JOIN u.friends uf JOIN uf.posts pf WHERE u.id = :userId) 
            OR p.id IN ( SELECT pg.id FROM User u JOIN u.myGroups gf JOIN gf.posts pg WHERE u.id = :userId AND p.author.id <> :userId) 
            ORDER BY p.date DESC
            """)
    List<Post> getBoardPosts(@Param("userId") Integer userId);

}
