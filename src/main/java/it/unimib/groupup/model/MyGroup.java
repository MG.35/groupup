package it.unimib.groupup.model;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.*;

/**
 * This class model a group
 */
@Entity
public class MyGroup {
    /**
     * The id of the group
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * The name of the group
     */
    private String name;

    /**
     * The description of the group
     */
    private String description;

    /**
     * The path of the photo in the filesystem
     */
    private String photo;

    /**
     * Users who joined the group
     */
    @ManyToMany
    @JoinTable(name = "my_group_users",
            joinColumns = @JoinColumn(name = "my_group_id"),
            inverseJoinColumns = @JoinColumn(name = "users_id"))
    private Set<User> users = new LinkedHashSet<>();

    /**
     * The group's posts
     */
    @OneToMany(mappedBy = "myGroup")
    @OrderBy("date DESC")
    private List<Post> posts = new ArrayList<>();

    /**
     * Empty constructor
     */
    public MyGroup() {
    }

    /**
     * Constructor with parameters
     * @param name name of the group
     * @param description description of the group
     */
    public MyGroup(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Get the id of the group
     * @return id of the group
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the id of the group
     * @param id id of the group
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the name of the group
     * @return name of the group
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the group
     * @param name name of the group
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the description of the group
     * @return description of the group
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the group
     * @param description description of the group
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the users of the group
     * @return users of the group
     */
    public Set<User> getUsers() {
        return users;
    }

    /**
     * set the users of the group
     * @param users users of the group
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /**
     * Get the posts of the group
     * @return posts of the group
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * Set the posts of the group
     * @param posts posts of the group
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * Get the photo of the group
     * @return photo of the group
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * Set the photo of the group
     * @param photo photo of the group
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * Check if the object is equal to the current object
     * @param o object to compare
     * @return true if the objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MyGroup myGroup = (MyGroup) o;
        return id != null && Objects.equals(id, myGroup.id);
    }

    /**
     * Get the hash code of the object
     * @return hash code of the object
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    /**
     * Get the string representation of the object
     * @return string representation of the object
     */
    @Override
    public String toString() {
        return "MyGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}