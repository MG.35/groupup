package it.unimib.groupup.model;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Entity that represents a user comment
 */
@Entity
public abstract class Comment {

    /**
     * The id of the comment
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The sentence of the comment
     */
    private String sentence;

    /**
     * Users that likes this comment
     */
    @ManyToMany
    @JoinTable(name = "liked_comments",
            joinColumns = @JoinColumn(name = "comment_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> likedBy = new LinkedHashSet<>();

    /**
     * User author of the comment
     */
    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    /**
     * Empty constructor
     */
    public Comment() {
    }

    /**
     * Constructor with parameters
     * @param sentence - the sentence
     */
    public Comment(String sentence) {
        this.sentence = sentence;
    }

    /**
     * Getter for the id
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for the id
     * @param id - the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for the sentence
     * @return the sentence
     */
    public String getSentence() {
        return sentence;
    }

    /**
     * Setter for the sentence
     * @param sentence - the sentence
     */
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    /**
     * Getter for the likedBy
     * @return the likedBy
     */
    public Set<User> getLikedBy() {
        return likedBy;
    }

    /**
     * Setter for the likedBy
     * @param likedBy - the likedBy
     */
    public void setLikedBy(Set<User> likedBy) {
        this.likedBy = likedBy;
    }

    /**
     * Getter for the author
     * @return the author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Setter for the author
     * @param author - the author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * To string method
     * @return a string describing the comment
     */
    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", sentence='" + sentence + '\'' +
                '}';
    }

}
