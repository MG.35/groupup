package it.unimib.groupup.model;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SAVED_POST")
public class SavedPost {

    /**
     * The id of the post
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * The user that saved the post
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * The post that was saved
     */
    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    /**
     * The label of the post
     */
    @ManyToOne
    @JoinColumn(name = "label_id")
    private MyLabel label;

    /**
     * empty constructor
     */
    public SavedPost() {
    }

    /**
     * Get the ID
     * @return the unique ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID
     * @param id the unique ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the user that saved the post
     * @return the user that saved the post
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user that saved the post
     * @param user the user that saved the post
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the post that was saved
     * @return the post that was saved
     */
    public Post getPost() {
        return post;
    }

    /**
     * Set the post that was saved
     * @param post the post that was saved
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * Get the label of the post
     * @return the label of the post
     */
    public MyLabel getLabel() {
        return label;
    }

    /**
     * Set the label of the post
     * @param label the label of the post
     */
    public void setLabel(MyLabel label) {
        this.label = label;
    }

    /**
     * Compare two saved posts
     * @param o the saved post to compare
     * @return if the saved posts are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SavedPost savedPost = (SavedPost) o;
        return id != null && Objects.equals(id, savedPost.id);
    }

    /**
     * Hash the entity
     * @return A hash of the entity
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
