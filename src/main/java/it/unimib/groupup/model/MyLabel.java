package it.unimib.groupup.model;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "MY_LABELS")
public class MyLabel {

    /**
     * The id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * The string of the label
     */
    private String label;

    /**
     * The user that created the label
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * The posts associated with the label
     */
    @OneToMany(mappedBy = "label", cascade = CascadeType.REMOVE)
    private Set<SavedPost> savedPosts = new LinkedHashSet<>();

    /**
     * Empty constructor
     */
    public MyLabel() {
    }

    /**
     * Get the id
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the id
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the label
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set the label
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Get the user
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the posts associated with the label
     * @return the posts associated with the label
     */
    public Set<SavedPost> getSavedPosts() {
        return savedPosts;
    }

    /**
     * Set the posts associated with the label
     * @param savedPosts the posts to set
     */
    public void setSavedPosts(Set<SavedPost> savedPosts) {
        this.savedPosts = savedPosts;
    }

    /**
     * Check if the object is equal to the current object
     * @param o object to compare
     * @return true if the objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MyLabel myLabels = (MyLabel) o;
        return id != null && Objects.equals(id, myLabels.id);
    }

    /**
     * Get the hash code of the object
     * @return hash code of the object
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
