package it.unimib.groupup.model;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This class model the CommentRoot
 */
@Entity
public class CommentRoot extends Comment{

    /**
     * The Post that this comment refers
     */
    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    /**
     * The leaf comments of this root comment
     */
    @OneToMany(mappedBy = "commentRoot", cascade = CascadeType.REMOVE)
    private Set<CommentLeaf> comments = new LinkedHashSet<>();

    /**
     * Empty constructor
     */
    public CommentRoot() {
    }

    /**
     * Constructor with parameters
     * @param sentence the sentence of the comment
     */
    public CommentRoot(String sentence) {
        super(sentence);
    }

    /**
     * Getter of the post
     * @return the post
     */
    public Post getPost() {
        return post;
    }

    /**
     * Setter of the post
     * @param post the post
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * Getter of the comments
     * @return the comments
     */
    public Set<CommentLeaf> getComments() {
        return comments;
    }

    /**
     * Setter of the comments
     * @param comments the comments
     */
    public void setComments(Set<CommentLeaf> comments) {
        this.comments = comments;
    }

}
