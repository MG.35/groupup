package it.unimib.groupup.model;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This class represents a user post
 */
@Entity
public class Post {

    /**
     * The id of the post
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The post message
     */
    private String message;

    /**
     * Date of publication of the post
     */
    private Date date;

    /**
     * The group in which it was posted
     */
    @ManyToOne
    @JoinColumn(name = "my_group_id")
    private MyGroup myGroup;

    /**
     * The author of the post
     */
    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    /**
     * The users that likes this post
     */
    @ManyToMany
    @JoinTable(name = "liked_post",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "users_id"))
    private Set<User> likedBy = new LinkedHashSet<>();

    /**
     * The comments of this post
     */
    @OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
    private Set<CommentRoot> comments = new LinkedHashSet<>();

    /**
     * The saved posts related to this post
     */
    @OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
    private Set<SavedPost> savedPosts = new LinkedHashSet<>();

    /**
     * Empty constructor
     */
    public Post() {
        this.date = new Date();
    }

    /**
     * Constructor with parameters
     *
     * @param message the message of the post
     */
    public Post(String message) {
        this.date = new Date();
        this.message = message;
    }

    /**
     * Get the id of the post
     *
     * @return the id of the post
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the id of the post
     *
     * @param id the id of the post
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the message of the post
     *
     * @return the message of the post
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the message of the post
     *
     * @param message the message of the post
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get the date of the post
     *
     * @return the date of the post
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the date of the post
     *
     * @param date the message of the post
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the group of the post
     *
     * @return the group of the post
     */
    public MyGroup getMyGroup() {
        return myGroup;
    }

    /**
     * Set the group of the post
     *
     * @param myGroup the group of the post
     */
    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    /**
     * Get the author of the post
     *
     * @return the author of the post
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Set the author of the post
     *
     * @param author the author of the post
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Get the users who liked the post
     *
     * @return the users who liked the post
     */
    public Set<User> getLikedBy() {
        return likedBy;
    }

    /**
     * Set the users who liked the post
     *
     * @param likedBy the users who liked the post
     */
    public void setLikedBy(Set<User> likedBy) {
        this.likedBy = likedBy;
    }

    /**
     * Get the comments of the post
     *
     * @return the comments of the post
     */
    public Set<CommentRoot> getComments() {
        return comments;
    }

    /**
     * Set the comments of the post
     *
     * @param comments the comments of the post
     */
    public void setComments(Set<CommentRoot> comments) {
        this.comments = comments;
    }


    /**
     * Get the saved posts related to this post
     * @return the saved posts related to this post
     */
    public Set<SavedPost> getSavedPosts() {
        return savedPosts;
    }

    /**
     * Set the saved posts related to this post
     * @param savedPosts the saved posts related to this post
     */
    public void setSavedPosts(Set<SavedPost> savedPosts) {
        this.savedPosts = savedPosts;
    }

    /**
     * Check if two posts are equal
     *
     * @param o the other post
     * @return true if the posts are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Post post = (Post) o;
        return id != null && Objects.equals(id, post.id);
    }

    /**
     * Get the hashcode of the post
     *
     * @return the hashcode of the post
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    /**
     * Get the string representing the post
     *
     * @return the string representing the post
     */
    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }
}
