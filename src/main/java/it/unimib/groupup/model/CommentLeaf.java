package it.unimib.groupup.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This class models the comment leaf
 */
@Entity
public class CommentLeaf extends Comment{

    /**
     * The root comment
     */
    @ManyToOne
    @JoinColumn(name = "comments_id")
    private CommentRoot commentRoot;


    /**
     * Empty constructor
     */
    public CommentLeaf() {
    }

    /**
     * Constructor with parameters
     * @param sentence the sentence of the comment
     */
    public CommentLeaf(String sentence) {
        super(sentence);
    }

    /**
     * Get the comment root
     * @return the comment root
     */
    public CommentRoot getCommentRoot() {
        return commentRoot;
    }

    /**
     * Set the comment root
     * @param commentRoot the comment root
     */
    public void setCommentRoot(CommentRoot commentRoot) {
        this.commentRoot = commentRoot;
    }

    /**
     * Get the comments
     * @return the comments
     */
    public CommentRoot getComments() {
        return commentRoot;
    }

    /**
     * Set the comments
     * @param comments the comments
     */
    public void setComments(CommentRoot comments) {
        this.commentRoot = comments;
    }
}
