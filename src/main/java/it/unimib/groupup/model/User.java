package it.unimib.groupup.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.Hibernate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.persistence.*;
import java.util.*;

/**
 * Entity that represents a user
 */
@Entity
@Table(name = "MYUSER")
public class User {

    /**
     * Hash for password
     */
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();


    /**
     * The id of the user
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * The name of the user
     */
    private String name;

    /**
     * The surname of the user
     */
    private String surname;

    /**
     * The user email
     */
    private String email;

    /**
     * The user password. It is not returned by the API
     */
    private @JsonIgnore String password;

    /**
     * The username of the user
     */
    private String username;

    /**
     * The path of the photo in the filesystem
     */
    private String photo;

    /**
     * The group in which the user joined
     */
    @ManyToMany(mappedBy = "users")
    private Set<MyGroup> myGroups = new LinkedHashSet<>();

    /**
     * The friends of the user
     */
    @ManyToMany
    @JoinTable(name = "friends",
            joinColumns = @JoinColumn(name = "user_1_id"),
            inverseJoinColumns = @JoinColumn(name = "users_2_id"))
    private Set<User> friends = new LinkedHashSet<>();

    /**
     * The posts that the user has published
     */
    @OneToMany(mappedBy = "author")
    @OrderBy("date DESC")
    private List<Post> posts = new ArrayList<>();

    /**
     * The comments liked by the user
     */
    @ManyToMany(mappedBy = "likedBy")
    private Set<Comment> likedComments = new LinkedHashSet<>();

    /**
     * The comments that the user has made
     */
    @OneToMany(mappedBy = "author")
    private Set<Comment> myComments = new LinkedHashSet<>();

    /**
     * The posts liked by the user
     */
    @ManyToMany(mappedBy = "likedBy")
    private Set<Post> likedPost = new LinkedHashSet<>();

    /**
     * The saved posts of the user
     */
    @OneToMany(mappedBy = "user")
    private Set<SavedPost> savedPosts = new LinkedHashSet<>();

    /**
     * The labels that the user has created
     */
    @OneToMany(mappedBy = "user")
    private Set<MyLabel> myLabels = new LinkedHashSet<>();

    /**
     * The security roles of the user
     */
    private String[] roles = new String[4];

    /**
     * Empty constructor
     */
    public User() {
    }

    /**
     * Constructor with params
     * @param name - the user's name
     * @param surname - the user's surname
     * @param email - the user's mail
     * @param password the user's password
     * @param username the user's username
     * @param photo the user's photo
     *
     */
    public User(String name, String surname, String email, String password, String username, String photo, String... roles) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = PASSWORD_ENCODER.encode(password);
        this.username = username;
        this.photo = photo;
        this.roles = roles;
    }

    /**
     * Get the ID
     * @return the unique ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID
     * @param id - the unique ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the name
     * @return the user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name - the user's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the surname
     * @return the user's surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set the surname
     * @param surname - the user's surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Get the mail
     * @return the mail
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the mail
     * @param email - the user's mail
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password
     * @param password - the user's password
     */
    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    /**
     * Get the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username
     * @param username - the user's username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the photo
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * Set the photo
     * @param photo - the user's photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * Get the groups
     * @return the groups
     */
    public Set<MyGroup> getMyGroups() {
        return myGroups;
    }

    /**
     * Set the groups
     * @param myGroups - the user's groups
     */
    public void setMyGroups(Set<MyGroup> myGroups) {
        this.myGroups = myGroups;
    }

    /**
     * Get the friends
     * @return the friends
     */
    public Set<User> getFriends() {
        return friends;
    }

    /**
     * Set the friends
     * @param friends - the user's friends
     */
    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }

    /**
     * Get the posts
     * @return posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * Set the posts
     * @param posts - the user's posts
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * Get the liked comments
     * @return comments
     */
    public Set<Comment> getLikedComments() {
        return likedComments;
    }

    /**
     * Set the liked comments
     * @param liked - the comments liked
     */
    public void setLikedComments(Set<Comment> liked) {
        this.likedComments = liked;
    }

    /**
     * Get the user comments
     * @return comments
     */
    public Set<Comment> getMyComments() {
        return myComments;
    }

    /**
     * Set the user comments
     * @param myComments - the user comments
     */
    public void setMyComments(Set<Comment> myComments) {
        this.myComments = myComments;
    }

    /**
     * get the liked posts of the user
     * @return comments
     */
    public Set<Post> getLikedPost() {
        return likedPost;
    }

    /**
     * Set the user comments
     * @param likedPost - the user comments
     */
    public void setLikedPost(Set<Post> likedPost) {
        this.likedPost = likedPost;
    }


    /**
     * Get the saved posts of the user
     * @return the saved posts
     */
    public Set<SavedPost> getSavedPosts() {
        return savedPosts;
    }

    /**
     * Set the saved posts of the user
     * @param savedPosts the saved posts
     */
    public void setSavedPosts(Set<SavedPost> savedPosts) {
        this.savedPosts = savedPosts;
    }

    /**
     * Get the labels that the user has created
     * @return the labels that the user has created
     */
    public Set<MyLabel> getMyLabels() {
        return myLabels;
    }

    /**
     * Set the labels that the user has created
     * @param myLabels the labels that the user has created
     */
    public void setMyLabels(Set<MyLabel> myLabels) {
        this.myLabels = myLabels;
    }

    /**
     * Get the roles
     * @return the roles
     */
    public String[] getRoles() {
        return roles;
    }

    /**
     * Set the roles
     * @param roles - the user's roles
     */
    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    /**
     * Compare two users
     * @param o - the user to compare
     * @return If users are the same entity
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return id != null && Objects.equals(id, user.id);
    }

    /**
     * Hash the entity
     * @return An hash of the entity
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    /**
     * To string method
     * @return A description of the entity
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

}
