package it.unimib.groupup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupupApplication {

    /**
     * The starting point for running the application
     */
    public static void main(String[] args) {
        SpringApplication.run(GroupupApplication.class, args);
    }

}
